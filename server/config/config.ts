// will convert to using node_env soon
export const CONFIG =  {
    dbType: 'pg',
    db: {
        pg: {
            user: 'postgres',
            database: 'safereports', 
            password: 'hereliesafreeelf215',
            host: 'localhost',
            port: 5432, 
            max: 10, 
            idleTimeoutMillis: 30000,
        },
        mssql: {
            user: 'sa',
            password: 'hereliesafreeelf215',
            server: 'localhost',
            database: 'safereports',
            pool: {
                max: 10,
                min: 0,
                idleTimeoutMillis: 30000
            }
        },
    }
};