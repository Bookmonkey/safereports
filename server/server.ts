declare var __dirname;

import * as path from "path";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as passport from "passport";
import * as session from 'express-session';
import * as helmet from 'helmet';
import * as compression from "compression";
import * as cookieParser from "cookie-parser";
import * as morgan from "morgan";
import * as cors from "cors";

import * as ejs from "ejs";
// var history = require("connect-history-api-fallback");

let port = 8080;
const app = express();
app.use(helmet());
app.use(compression());

app.use(morgan("dev")); // log requests in console

app.use(cookieParser()); // read cookies
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());


// api routes defintions
import { AuthController } from './auth';
import { PeopleController } from './routes/people';
import { SafetyController } from './routes/safety';
import { AssetsController } from './routes/assets';
import { SuppliersController } from './routes/suppliers';
import { DocumentsController } from './routes/documents';
import { UsersController } from './routes/users';
import { AdminController} from './routes/admin';

import { DashboardController } from "./routes/dashboard";
import { DraftsController } from "./routes/drafts";

import { MssqlStore, PgStore, SqlStore } from './db/db';

import { CONFIG } from './config/config';
let StoreFactory = () => {
    switch (CONFIG.dbType){
        case "pg": return new PgStore();
        case "mmsql": return new MssqlStore();
    }
};

// either mmsql or pg
const dbConnection: SqlStore =  StoreFactory();

app.use(cors({
    origin:['http://localhost:80', 'http://localhost:3000'],
    methods:['GET','POST'],
    credentials: true // enable set cookie
}));

// Allows the .html suffix and renders by using ejs.
app.engine('html', ejs.renderFile);
app.set('views', __dirname);
app.set('view engine', 'ejs'); // set up templates

app.use("/js", express.static(path.join(__dirname, "js")));
app.use("/css", express.static(path.join(__dirname, "css")));
// app.use("/dist", express.static(path.join(__dirname)));



var date = new Date();
date.setTime(date.getTime() + (5 * 24 * 60 * 60 * 1000));

app.use(session({
    secret: '&&dsdsfdffdddffd582dfhdf##^assf155885697143',
})); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions

// Only shows the /public folder if they are logged in.
// app.use('/public', function(req, res, next) {
//   if (req.isAuthenticated()) next(); 
//   else res.status(403).send("Forbidden access");
// }, express.static(path.join(__dirname, 'public')));


app.use('/api/auth', AuthController(dbConnection, passport));

app.use('/api/people', PeopleController(dbConnection));
app.use('/api/safety', SafetyController(dbConnection));
app.use('/api/assets', AssetsController(dbConnection));
app.use("/api/suppliers", SuppliersController(dbConnection));
app.use('/api/documents', DocumentsController(dbConnection));
app.use('/api/users', UsersController(dbConnection));
app.use('/api/admin', AdminController(dbConnection));
app.use("/api/dashboard", DashboardController(dbConnection));

app.use("/api/drafts", DraftsController(dbConnection));


//  must be last route 
app.get("/*", (req, res) => {
  res.render("index.html");
});

app.listen(port);
console.log(`Server on ${port}`);



const stripe = require("stripe")("KEY_HERE");
// demo
app.get("/api/billing/create/plan", (req, res) => {
    stripe.plans.create({
        name: "Basic Plan",
        id: "basic-monthly",
        interval: "month",
        currency: "nzd",
        amount: 30,
    }, (err, plan) => {
        console.log(plan);
    });
});

app.get("/api/billing/create/customer", (req, res) => {
    stripe.customers.create({
        email: "bookmonkey95@gmail.com",
    }, (err, customer) => {
    // asynchronously called
        console.log(customer);
    });
});

app.get("/api/billing/subscription", (req, res) => {
    stripe.subscriptions.create({
        customer: "cus_BNXDhOHWHs9zev",
        items: [
            {
                plan: "basic-monthly",
            },
        ],
        trial_period_days: 14,
    }, function(err, subscription) {
    // asynchronously called
        console.log(subscription);
    });
})
    