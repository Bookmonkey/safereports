interface DataItem {
    id: string,
    name: string,
    grouping?: string
};

export class Common {
    static transformJSONObject(object) {
        return `{ ${object} }`;
    }


    /**
     * All data items should be objects
     * 
     * @static
     * @param {any} id 
     * @param {any} name 
     * @param {any} [grouping] 
     * @memberof Common
     */
    static createDataItem(id, name, grouping?){
        let dataItem: DataItem = {
            id: id,
            name: name
        };

        if(grouping){
            dataItem.grouping = grouping;
        }


        return dataItem;
        
    }
}
