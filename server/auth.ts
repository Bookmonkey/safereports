import { Router, Request, Response, NextFunction } from 'express';
import { SqlStore } from './db/db';
import * as bcrypt from 'bcrypt-nodejs';
var passportLocal = require("passport-local");
import * as async from "async";

import { IUserRequest } from './interfaces/express'

const LocalStrategy = passportLocal.Strategy;

export function AuthController(db, passport) {
    const router: Router = Router();
    passport.serializeUser((user, done) => {
        done(null, user.id)
    });
    
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        db.getUserById(id)
        .then(async response => {
            let user = response.data[0];
            let modules = await db.getUserModulesById(user.id);
            user.modules = modules.data;
            done(null, user);
        })
        .catch(e => console.log(e));
    });
    // used to serialize the user for the session

    passport.use('login', new LocalStrategy((username, password, done) => {
        db.getUserByUsername(username)
        .then(async user => {
            user = user.data[0];
            let modules = await db.getUserModulesById(user.id);
            user.modules = modules.data;
            // console.log(password, user.password);
            // console.log(bcrpt.compareSync(password, user.password));
            if (!user) {
                done(null, false, 'Unknown user');
            } else if (!bcrypt.compareSync(password, user.password)) {
                done(null, false, 'Wrong password');
            } else {
                done(null, user);
            }
        })
        .catch(e => done(null, false, e.name + " " + e.message));
    }));


    router.post('/login', function(req: IUserRequest, res: Response, next: NextFunction){
        passport.authenticate('login', (err, userInfo, info) =>{
			if (err) {
            	next(err);
			}

			if (!userInfo) {
                res.status(400).send('Incorrect username/password combination');
                return;
            }
            else {
                req.logIn(userInfo, (err) => {
                    if (err) return next(err); 
                    const user = {
                        id: userInfo.id,
                        username: userInfo.username,
                        person_id: userInfo.person_id,
                        account_id: userInfo.account_id,
                        is_developer: userInfo.is_developer,
                        modules: userInfo.modules,
                    };

                    req.user = user;                
                    res.status(200).send(user);
                    return;
                });
            }
		})(req, res, next);
    });
    router.get("/logout", (req: IUserRequest, res: Response) => {
		req.session.destroy(function (err) {
			res.status(200).send("logged out"); 
		});
	});

    
    router.get("/loginStatus", (req: IUserRequest, res: Response) => {
        if(req.isAuthenticated()){
			res.status(200).send({
                id: req.user.id,
                username: req.user.username,
                person_id: req.user.person_id,
                account_id: req.user.account_id,
                is_developer: req.user.is_developer,
                modules: req.user.modules
            });
			return;
		}
        else{
            res.status(400).send(null);
        }
    });

    return router;    
};