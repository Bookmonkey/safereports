export interface DataCollection{}
export interface DataItem{
    id: Number,
    name: String,
    account_id: Number
    grouping_id:Number,
    sub_grouping_id: Number
};