export interface Person {
    first_name,
    last_name: String,
    birthdate: Date,
    gender: String,
    department: any,
    position: any,
    archived: boolean,
    shift?: any, // optional
    location?: any,
    phone?: String,
    email?:String,
    general_notes?: String, // optional
    special_notes?: String, // optional
    account_id: Number,
}

export interface Training {
    id?: number,
    training_id: number,
    acquired_on: Date,
    expires_on: Date,
    notes: string,
    person_id: number
}

export interface Absence {
    id?: number,
    
}