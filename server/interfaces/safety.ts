export interface RegisterItem {
    id: number,
    hazard: String,
    potential_harm: String,
    hazard_mitigation:String,
    controls: String,
    training_required: Boolean,
    risk_factor: String,
    location: Number,
    review_date: Number,
    date_reported: Date,
    account_id: Number
}


export interface SafetyReport {
    id: Number,
    type: String, 
    date_occurred: Date, 
    location: String, 
    department: String, 
    what_happened: String, 
    employee_id: Number, 
    supervisor_id: String, 
    damage_description: String, 
    damages: String,
    harm_description: String,
    harm: String,
    risk_likelihood: String, 
    risk_harm: String, 
    corrective_actions: String, 
    issue_still_present: Boolean, 
    account_id: Number
};