import { Router, Request, Response, NextFunction } from 'express';

export interface IUserRequest extends Request {
  user: {
    id: number;
    username: string;
    account_id: number;
    person_id: number;
    is_developer: boolean;
    modules: Array<string>;
  };
  logIn: any;
  session: any;
  isAuthenticated: any;
}


export interface IUserResponse extends Response {
    user: any,
    logIn:any,
    session: any,
    isAuthenticated: any,
}
