export interface User {
  id?: string;
  email: String;
  username: String;
  mobile_phone: String;
  mobile_number: String;
  is_developer?: boolean;
  password: String;
  person: Number;
  modules: Array<Number>;
  account_id: Number;
};