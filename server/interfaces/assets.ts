
export interface Asset {
  id: Number;
  type: Text;
  assetID: Text;
  asset_id: Text;
  location: Text;
  comments: Text;
  manfacture_date?: Date;
  wof_date?: Date;
  cof_date?: Date;
  next_service_date?: Date;
}
