
import { Router, Request, Response } from 'express';
import {SqlStore} from '../db/db';

import { IUserRequest } from '../interfaces/express';
export function UsersController(db) {

    const router: Router = Router();
    // Modules and Permissions
    router.get("/settingsmodules", (req: Request, res: Response) => {
        const sqlObject = {
            statement: `select * from settings_modules;`,
        };

        db.getUserSettingsModules()
        .then(result => res.status(200).send(result.data))
        .catch(err => console.trace(err));
    });

    router.get("/settingspermissions", (req:Request, res: Response) => {
        const sqlObject = {
            statement: `select * from settings_permissions;`,
        };

        db.getUserSettingsPermissions();
    });


    // Users 

    router.post("/add", (req: IUserRequest, res: Response) => {
        let user = req.body.user;

        user.person = user.person.id;
        user.account_id = req.user.account_id;
        
        user.modules = user.modules.map(ele => ele.id);

        db.addUser(user)
        .then(response => {
            if(user.modules.length > 0){
                db.addModulesToUser(response.data[0].id, user.modules)
                .then(response => res.status(200).send(response))
                .catch(err => console.error(err));

                return;
            }
            res.status(200).send(response);
        })
        .catch(err => {
            console.log(err);
        });
    });

    router.post("/add/modules/:userId", (req: IUserRequest, res: Response) => {
        let userId = req.params.userId;
        const modules = req.body.modules.map(ele => ele.id);

        db.addModulesToUser(userId, modules)
        .then(response => res.status(200).send(response))
        .catch(err => console.error(err));
    });

    
    router.get("/add/module/:userId/:moduleId", (req: IUserRequest, res:Response) => {
        let userId = req.params.userId;
        let moduleId = req.params.moduleId;

        db.addeModuleToUser(userId, moduleId)
        .then(response => res.status(200).send(response))
        .catch(err => console.error(err));
    })

    router.get("/remove/module/:userId/:moduleId", (req: IUserRequest, res:Response) => {
        let userId = req.params.userId;
        let moduleId = req.params.moduleId;

        db.removeModuleFromUser(userId, moduleId)
        .then(response => res.status(200).send(response))
        .catch(err => console.error(err));
    })

    router.post("/update", (req: Request, res: Response) => {
        let user = req.body.user;
        db.updateUser(user)
        .then(response => {
            res.status(200).send("OK");
        })
        .catch(error => {
            console.error(error);
        });
    });

    router.get("/get", (req: IUserRequest, res: Response) => {
        const accountID = req.user.account_id;
        db.getUsers(accountID)
        .then(response => {
            console.log(response.data);
            res.status(200).send(response.data)
        })
        .catch(err => {
            console.log(err);
        });
    });

    router.get("/get/:id", async (req: Request, res: Response) => {
        const id = req.params.id;
        try{
            let user = await db.getUserById(id);
            let modules = await db.getUserModulesById(id);

            user.data[0].modules = modules.data;
            
            res.status(200).send(user.data);
        }
        catch (err) {
            console.log(err);
        }
    });

    router.get("/archive/:id", (req: Request, res: Response) => {
        //db.getUsers();
    });

    router.get("/delete/:id",  async (req: Request, res: Response) => {
        const id = req.params.id;
        try {
            let deletedUserModules = await db.deleteModulesOnUserID(id);

            if(deletedUserModules){
                let deletedUser = await db.deleteUserById(id);
                if(deletedUser.error === false) {
                    res.status(200).send(deletedUser);
                }
                else {
                    res.status(400).send("Unable to delete user");
                }
            }
            else{
                res.status(400).send("Unable to delete user modules");
            }
        }
        catch (err){
            console.error(err);
        }
    });
    
    return router;
};