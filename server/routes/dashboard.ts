import { Router, Request, Response } from "express";
import { SqlStore } from "../db/db";
import { IUserRequest, IUserResponse } from "../interfaces/express";
import * as moment from "moment";
let months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];
export function DashboardController(db) {
  const router: Router = Router();

  let formatStats = stats => {
    let total: number = 0;
    let graph = {
      labels: [],
      data: []
    };
    let label;
    stats.data.map(ele => {
      total += parseInt(ele.count);
      let label = `${months[ele.month - 1]} '${ele.year}`
      graph.labels.push(label);
      graph.data.push(ele.count);
    });

    if (graph.data.length === 0) {
      graph.labels = ["Start", "End"];
      graph.data = [0, 0];
    }

    // if there is only 1 data point, display it as a line.
    // Otherwise it will just display a single dot on the graph.
    if (graph.data.length === 1) {
      graph.labels.push(graph.labels[0]);
      graph.data.push(graph.data[0]);
    }

    return {
      total: total,
      graph: graph
    };
  };

  router.get("/stats/:period", async (req: IUserRequest, res: Response) => {
    let period = req.params.period;

    try {
      let safetyStats = await db.getDashboardStatsForSafety(period);
      safetyStats = formatStats(safetyStats);

      let hazardStats = await db.getDashboardStatsForHazards(period);
      hazardStats = formatStats(hazardStats);

      let dataObject = {
        safety: safetyStats,
        hazard: hazardStats
      };

      res.status(200).send(dataObject);
    } catch (err) {
      console.log(err);
    }

    // db.getDashboardStats(period).then(result => {
    //   console.log(result.data);
    //   res.status(200).send(result.data);
    // });
  });

  router.get("/notifications/all", async (req: IUserRequest, res: IUserResponse) => {
    function getNotifications() {
      return db.getNotifications();
    }
    try {

      let data = await getNotifications();
      data = data.data;

      let notifications = {
        asset: [],
        hazard: [],
        people: [],
      };

      let link = "";

      data.map(ele => {
        if (ele.type === "Asset") {
          link = `/app/assets/view/${ele.id}`;
        } else {
          link = `/app/registers/view/${ele.id}`;
        }

        ele.type = ele.type.toLowerCase();
        notifications[ele.type].push({
          notificationType: ele.type,
          notificationClass: "info",
          notificationTitle: `${ele.type} '${ele.title} (${ele.secondary})' is due for a ${ele.reason} on ${moment(ele.due_date).format(
            "DD/MM/YYYY"
          )}`,
          notificationLink: { linkTitle: "View", link: link }
        });
      });

      res.status(200).send(notifications);
    } catch (error) {
      console.error(error);
    }
  })

  router.get("/notifications/summary", async (req: IUserRequest, res: IUserResponse) => {
      function getSummaryNotifications() {
        return db.getSummaryNotifications();
      }
      try {
        let notifications = await getSummaryNotifications();
        res.status(200).send(notifications.data);

      } catch (error) {
        console.error(error);
      }
    }
  );

  return router;
}
