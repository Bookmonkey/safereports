import { Router, Request, Response } from 'express';

import {SqlStore} from '../db/db';
import { IUserRequest, IUserResponse } from '../interfaces/express';

import { Common } from '../helpers/common';

export function SafetyController(db: SqlStore) {

	const router: Router = Router();

	router.get("/get", (req: IUserRequest, res: Response) => {
		let accountID = req.user.account_id;
		db.getSafetyReports(accountID)
		.then((results) => {
			console.log(results.data);

			results.data = results.data.map(ele => {
				ele.type = ele.type.join('/');
				ele.department = Common.createDataItem(ele.department, ele.department_name);
				ele.position = Common.createDataItem(ele.position, ele.position_name);
				ele.shift = Common.createDataItem(ele.shift, ele.shift_name);
				ele.location = Common.createDataItem(ele.location, ele.location_name);
				
				return ele;
			})
			res.status(200).send(results.data);
		})
		.catch((err) => {
			console.trace(err);
		});
	});

	router.get("/get/:id", async (req, res) => {
		const id = req.params.id;
		let getSafetyReportById = (id) => {
			return db.getSafetyReportById(id)
			.then(results => {

				let safety = results.data[0];

				safety.type = {
					readable: safety.type.join('/'),
					array: safety.type,
				};

				safety.damages = {
					readable: safety.damages.join("/"),
					array: safety.damages,
				};

				safety.harm = {
					readable: safety.harm.join("/"),
					array: safety.harm
				};

				safety.location = Common.createDataItem(safety.location, safety.location_name);
				safety.department = Common.createDataItem(safety.department, safety.department_name);
				safety.employee = Common.createDataItem(safety.employee_id, safety.employee);
				safety.supervisor = Common.createDataItem(safety.supervisor_id, safety.supervisor);	

				return safety;
			})
			.catch((err) => {
				throw err;
			});
		}

		let getInvestigation = (id) => {
			return db.getSafetyReportInvestigation(id)
			.then(response => {
				return response.data[0];
			});
		};

		let getHazardLinksOnSafetyReport = (id) => {
			return db.getHazardLinksOnSafetyReport(id);
		};
		
		try {
			let safetyReport = await getSafetyReportById(id);
			let investigation = await getInvestigation(id);
			let hazardLinks = await getHazardLinksOnSafetyReport(id);

			res.status(200).send(
				{
					safety: safetyReport,
					investigation: investigation,
					links: hazardLinks.data
				}
			);
		} catch (error) {
			
		}
    });

	
	router.post("/add", (req: IUserRequest, res: Response) => {
		let safetyReport = req.body.safety;	
		console.log(safetyReport);
		// safetyReport.date_occurred = `'${safetyReport.date_occurred}`;
		safetyReport.location = safetyReport.location.id;
		safetyReport.department = safetyReport.department.id;
		safetyReport.employee_id = safetyReport.employee.id;
		safetyReport.supervisor_id = safetyReport.supervisor.id;

		safetyReport.type = Common.transformJSONObject(safetyReport.type);
		safetyReport.damages = Common.transformJSONObject(safetyReport.damages);
		safetyReport.harm = Common.transformJSONObject(safetyReport.harm);

		safetyReport.account_id = req.user.account_id;


		db.addSafetyReport(safetyReport)
		.then(results => {
			res.status(200).send(results.data);
		})
		.catch(error => {
			console.error(error);
		});
	});

	router.post("/update", (req: Request, res: Response) => {
		let safetyReport = req.body.safety;
		safetyReport.location = safetyReport.location.id;
		safetyReport.department = safetyReport.department.id;
		safetyReport.employee_id = safetyReport.employee.id;
		safetyReport.supervisor_id = safetyReport.supervisor.id;

		safetyReport.type = Common.transformJSONObject(safetyReport.type.array);
		safetyReport.damages = Common.transformJSONObject(safetyReport.damages.array);
		safetyReport.harm = Common.transformJSONObject(safetyReport.harm.array);
		
		db.updateSafetyReport(safetyReport)
		.then((results) => {
			res.status(200).send(results.data);
		})
		.catch((err) => {
			console.trace(err);
		});
	});

	router.post("/investigation/add", (req: IUserRequest, res: IUserResponse) => {
		let investigation = req.body.investigation;

		db.addIncidentInvestigation(investigation)
		.then(result => {
			res.status(200).send(result.data);
		})
		.catch(error => {
			console.error(error);
		});
	});

	router.post("/investigation/update", (req: IUserRequest, res: IUserResponse) => {
		let investigation = req.body.investigation;

		db.updateIncidentInvestigation(investigation)
		.then(result => {
			res.status(200).send(result.data);
		})
		.catch(error => {
			console.error(error);
		});
	});

	router.post("/links/add", (req:IUserRequest, res) => {
		let hazardLinks = req.body.hazardLinks;
		let userID = req.user.id;

		db.addHazardLinkToSafetyReport(hazardLinks, userID)
		.then(results => {
			console.log(results);
		});
	});

	router.get("/links/remove/:linkID", (req, res) => {
		let linkID = req.params.linkID;

		db.removeHazardLinkFromIncidentReport(linkID)
		.then(results => {
			res.status(200).send("OK");
		})
	})
	


	router.get("/archive/:id", (req, res) => {
		const id = req.params.id;
		// db.archiveSafetyReport(id)
		// .then((results) => {
		// 	res.status(200).send(results.data);
		// })
		// .catch((err) => {
		// 	console.trace(err);
		// });
	}); 


	router.get("/registers/view", (req:IUserRequest, res) => {
		const accountId = req.user.account_id;
		db.getHazardRegisters(accountId)
		.then(response => {
			response.data = response.data.map(ele => {
				ele.location = Common.createDataItem(ele.location, ele.location_name);
				console.log(ele);
				return ele;
			});
			res.status(200).send(response.data)
		});
	});

	router.get("/registers/view/:id", async (req: IUserRequest, res) => {
		const id = req.params.id;
		let getHazard = (id) => {
			return db.getHazardRegistersById(id)
			.then(response => {
				let register = response.data[0];
				register.location = Common.createDataItem(register.location, register.location_name);
				
				return register;
			});
		}
		let getHazardLinks = id => {
          return db.getIncidentsLinkedOnHazard(id).then(response => response.data);
		};
		
		try {
			let hazard = await getHazard(id);
			let linksOnIncidents = await getHazardLinks(id);

			res.status(200).send({
				hazard: hazard,
				linksOnIncidents: linksOnIncidents,
			});
		} catch (error) {
			console.log(error)
		}
	});


	router.post("/registers/add", (req: IUserRequest, res) => {
		let register = req.body.register;
		register.location = register.location.id;
		register.account_id = req.user.account_id;

		db.addHazardRegisterItem(register)
		.then(response => {
			res.status(200).send(response.data);
		})
		.catch(err => {
			console.trace(err);
		});
	});

	router.post("/registers/update", (req: IUserRequest, res) => {
		let register = req.body.register;
		register.location = register.location.id;

		
		db.updateHazardRegisterItem(register)
		.then(response => {
			res.status(200).send(response.data);
		})
		.catch(err => {
			console.trace(err);
		});
	});


	router.get("/registers/archive/:id/:state", (req: IUserRequest, res) => {
		let id = req.params.id;
		let state = req.params.state;
		// console.log(id);
		db.archiveRequestHazardRegisterItem(id, state).then(response => {
			res.status(200).send(state);
		});
	});

	router.get("/registers/delete/:id", (req: IUserRequest, res) => {
		let id = req.params.id;

		// db.deleteHazardRegisterItem(id).then(response => {});
	});

	
	return router;
};