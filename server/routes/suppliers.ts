import { Router, Request, Response } from "express";
import { SqlStore } from "../db/db";

import { IUserRequest, IUserResponse } from "../interfaces/express";
export function SuppliersController(db: SqlStore) {
  const router: Router = Router();

  router.get("/get", (req: IUserRequest, res: IUserResponse) => {
    let accountId = req.user.account_id;

    db.getSuppliers(accountId)
    .then(response => {
      res.status(200).send(response.data);
    })
    .catch(error => {
      console.error(error);
    });
  });
  router.get("/get/:id", (req: IUserRequest, res: IUserResponse) => {
    let accountId = req.user.account_id;
    let supplierId = req.params.id;

    db.getSupplierById(supplierId, accountId)
      .then(response => {
        res.status(200).send(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  });

  router.post("/add", (req: IUserRequest, res: IUserResponse) => {
    let accountId = req.user.account_id;
    let supplier = req.body.supplier;

    supplier.account_id = accountId;

    db.addSupplier(supplier)
      .then(response => {
        res.status(200).send(response.data);
      })
      .catch(error => {
        console.error(error);
      });
  });

  router.post("/update", (req: IUserRequest, res: IUserResponse) => {
    let supplier = req.body.supplier;

    db.updateSupplier(supplier)
    .then(response => {
      res.status(200).send(response.data);
    })
    .catch(error => {
      console.error(error);
    });
  });

  router.get("/delete/:id", (req: IUserRequest, res: IUserResponse) => {
    let id = req.params.id;
    let accountId = req.user.account_id;

    db.deleteSupplier(id, accountId).then(response => {
      res.status(200).send("OK");
    })
    .catch(error => console.error(error));
  });

  return router;
}
