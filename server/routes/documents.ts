import { Router, Request, Response } from 'express';

import * as async from 'async';
import {SqlStore} from '../db/db';

export function DocumentsController(db: SqlStore) {

    const router: Router = Router();

    router.get("/get", (req: Request, res: Response) => {
        db.getDocuments()
        .then((results) => {
            
        })
        .catch((err) => {

        })

        
    });

    
    router.post("/add", (req: Request, res: Response) => {
        let document = req.body.document;

        db.addDocument(document)
        .then((results) => {

        })
        .catch((err) => {
            
        })
        

    });




    return router;
};