import { Router, Request, Response } from "express";
import { SqlStore } from "../db/db";
import { Common } from "../helpers/common";

import { IUserRequest, IUserResponse } from "../interfaces/express";
export function AssetsController(db) {
  const router: Router = Router();

  router.get("/get",  (req: IUserRequest, res: IUserResponse) => {
    let accountID = req.user.account_id;
    db.getAssets(accountID)
    .then(result => {
      res.status(200).send(result.data);
    });
  });

  router.get("/get/:id", async (req:IUserRequest, res:IUserResponse) => {
    function getAssetById(id){
      return db.getAssetById(id)
        .then(result => {
          result.data = result.data[0];
          result.data.location = result.data.location = Common.createDataItem(result.data.location, result.data.location_name);
          return result.data;
        });
    };

    function getServiceLogsById(id) {
      return db.getServiceLogsById(id)
      .then(result => {
        return result.data;
      });
    };
    
    try {
      let id = req.params.id;

      let asset = await getAssetById(id);
      let service = await getServiceLogsById(id);
      res.status(200).send( { asset, service } );
    }
    catch (err) {
      console.error(err);
    }
  });

  router.post("/update", (req: IUserRequest, res: IUserResponse) => {
    let asset = req.body.asset;
    
    asset.location = asset.location.id;
    db.updateAsset(asset)
    .then(result => {
      res.status(200).send(result.data);
    })
  });

  router.post("/add", (req: IUserRequest, res: IUserResponse) => {
    let asset = req.body.asset;
    asset.location = asset.location.id;

    db.addAsset(asset).then(result => {
      res.status(200).send(result.data);
    })
    .catch(err => {
      console.error(err);
    });
  });

  router.get("/delete/:id", (req: IUserRequest, res: IUserResponse) => {
    let id = req.params.id;
    
    db.deleteAssetById(id).then(result => {
      res.status(200).send("OK");
    });
  });



  // services
  router.post("/add/service", (req: IUserRequest, res: IUserResponse) => {
    let userId = req.user.id;
    let accountId = req.user.account_id;

    let service = req.body.service;

    service.service_type = service.service_type.id;
    service.created_by = userId;
    service.account_id = accountId;
    
    db.addServiceLogToAsset(service)
    .then(response => {
      res.status(200).send("OK");
    });
  });

  return router;
}
