import { Router, Request, Response } from "express";
import { SqlStore } from "../db/db";

import { IUserRequest, IUserResponse } from "../interfaces/express";
export function DraftsController(db) {
  const router: Router = Router();
  router.get("/get/all/:location", (req: IUserRequest, res: IUserResponse) => {
    let userID = req.user.id;
    let location = req.params.location;
    db.getDraftsForUser(userID, location)
    .then(response => {
      res.status(200).send(response.data);
    });
  });


  router.get("/get/:id", (req: IUserRequest, res: IUserResponse) => {
    let draftId = req.params.id;
    let userId = req.user.id;

    db.getDraftById(draftId, userId)
    .then(response => {
      res.status(200).send(response.data);
    });
  });
  
  router.post("/add", (req: IUserRequest, res: IUserResponse) => {
    let draft = req.body.draft;
    let userId = req.user.id;

    draft.user_id = userId;

    db.addDraft(draft)
    .then(response => {
      res.status(200).send("OK");
    });
  });

  router.post("/update", (req: IUserRequest, res: IUserResponse) => {
    let draft = req.body.draft;
    db.updateDraft(draft)
    .then(response => {
      res.status(200).send("OK");
    });
  });

  router.get("/delete/:id", (req: IUserRequest, res: IUserResponse) => {
    let id = req.params.id;

    db.deleteDraftById(id)
      .then(response => {
        res.status(200).send("OK");
      })
      .catch(error => {
        console.error(error);
      })
  });
  return router;
}
