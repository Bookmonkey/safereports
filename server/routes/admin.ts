import { Router, Request, Response } from "express";
import { IUserRequest, IUserResponse } from "../interfaces/express";
import "babel-polyfill";
export function AdminController(db) {
  const router: Router = Router();
  // Data Collections
  // Gets only the data collections, not the data items
  router.get("/data/get/collections", (req, res) => {
    db
      .getDataCollectionsGroups()
      .then(result => {
        let data: Object = {};
        result.data.map((ele, index, self) => {
          if (ele.grouping_id === null) {
            data[ele.id] = {
              key: ele.key,
              name: ele.name,
              grouping_id: ele.grouping_id,
              id: ele.id,
              data: []
            };

            return;
          }

          for (const [key, value] of Object.entries(data)) {
            if (parseInt(key) === parseInt(ele.grouping_id))
              data[key].data.push(ele);
          }
        });
        result.data = data;
        res.status(200).send(result.data);
      })
      .catch(error => console.log(error));
  });

  // gets data_collection_items for a data_collection_group id
  router.get("/data/get/collection/:id", (req: IUserRequest, res) => {
    db
      .getDataCollectionsByGroupId(req.params.id, req.user.account_id)
      .then(result => {
        0;
        res.status(200).send(result.data);
      })
      .catch(error => console.log(error));
  });

  // Takes a array of strings that link to data_collection_group keys and returns them
  router.post("/data/get/collection/items", (req: IUserRequest, res) => {
    const dataCollections = req.body.collections;
    const accountID = req.user.account_id;
    
    db
      .getDataCollectionAndItems(dataCollections, accountID)
      .then(result => {
        let datasets = {};
        result.data.map((ele, index, self) => {
          if (datasets[ele.key] === undefined) datasets[ele.key] = [];
          datasets[ele.key].push(ele);
        });

        res.status(200).send(datasets);
      })
      .catch(error => console.log(error));
  });

  // Adds a data_collection_item to the data_collection_group
  router.post("/data/add/:collectionId", (req: IUserRequest, res) => {
    const newItem = req.body.item;
    const collectionId = req.params.collectionId;
    const accountID = req.user.account_id;

    db
      .addDataItemToCollection(collectionId, newItem, accountID)
      .then(result => {
        res.status(201).send(result.data[0]);
      })
      .catch(error => console.log(error));
  });

  router.post("/data/update/item", (req, res) => {
    const item = req.body.item;

    db.updateDataItem(item).then(result => {
      res.status(200).send(result.data[0]);
    });
  });

  router.get("/data/delete/item/:itemID/:accountID", (req, res) => {
    const itemID = req.params.itemID;
    const accountID = req.params.accountID;

    db.deleteDataItem(itemID, accountID).then(result => {
      res.status(200).send(result.data[0]);
    });
  });

  var stripe = require("stripe")("sk_test_lOOC6bqtlXVZojcgsrmBZaSJ");

  router.get("/list/plans", async (req, res) => {
    stripe.plans.list((err, plans) => {
      res.status(200).send(plans);
    });
  });

  // Accounts
  router.post("/register/account", async (req, res) => {
    let form = req.body.form;

    const createBusinessAccount = (planId) => {
      return db.createBusinessAccount(planId)
      .then(result => {
        return result.data[0].id;
      });
    }

    const createBusinessContact = (form, accountId) => {
      return db.createBusinessContact(form, accountId)
        .then(result => {
          return result.data[0].id;
        });
    }

    const createUserAccount = (user) => {
      return db.addUser(user)
        .then(result => {
          return result.data[0].id;
        });
    }

    const createPersonProfile = (person) => {
      return db.addPerson(person)
        .then(result => {
          return result.data[0].id;
        });
    };

    const addModulesToUser = async(userId) => {
      let modules = await db.getUserSettingsModules();
      modules = modules.data.map(ele => ele.id);
      db.addModulesToUser(userId, modules);
    }
    try {
      let planId = 1;
      let accountId = await createBusinessAccount(planId);
      let businessConactId = await createBusinessContact(form, accountId);


      let person = {
        first_name: form.userFirstName,
        last_name: form.userLastName,
        birthdate: null,
        gender: null,
        department: form.department,
        position: null,
        shift: null,
        location: null,
        general_notes: null,
        special_notes: null,
        phone: null,
        email: null,
        account_id: accountId,
      };

      let personId = await createPersonProfile(person);


      let user = {
        email: form.username,
        mobile_number: null,
        password: form.password,
        person: personId,
        account_id: accountId,
      };

      let userId = await createUserAccount(user);
      // console.log(addedModules);
      let addedModules = await addModulesToUser(userId);

    }
    catch (error) {
      console.error(error);      
    }


    // const createStripeCustomer = () => {
    //   let customer: any = {
    //     email: form.business_contact_email,
    //     description: form.business_contact_person
    //   };

    //   if (form.skipCreditCard === false) {
    //     customer.source = form.stripeToken;
    //   }

    //   return new Promise((resolve, reject) => {
    //     stripe.customers.create(customer, (err, customer) => {
    //       if (err) reject(err);
    //       else resolve(customer.id);
    //     });
    //   });
    // };

    // const subscribeToPlan = (customerId, planId) => {
    //   return new Promise((resolve, reject) => {
    //     stripe.subscriptions.create(
    //       {
    //         customer: customerId,
    //         items: [{ plan: planId }],
    //         trial_period_days: 30
    //       },
    //       (err, subscription) => {
    //         if (err) reject(err);
    //         else resolve(subscription);
    //       }
    //     );
    //   });
    // };

    // let customerId = await createStripeCustomer();
    // let subscription = await subscribeToPlan(customerId, form.plan);

    // create business account
    // create user account
    // send welcome email
  });

  router.get("/account", (req: IUserRequest, res: IUserResponse) => {
    const accountId = req.user.account_id;
    db
      .getBusinessAccountDetails(accountId)
      .then(result => {
        res.status(200).send(result.data);
      })
      .catch(err => {
        console.log(err);
      });
  });

  router.post("/account/update", (req: IUserRequest, res: IUserResponse) => {
      const account = req.body.account;
        console.log(account);
      db.updateBusinessAccountDetails(account)
      .then(result => {
          res.status(200).send(result);
      })
      .catch(err => {
          console.log(err);
      })
  });

  return router;
}
