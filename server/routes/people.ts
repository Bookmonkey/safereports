import { Router, Request, Response } from 'express';

import { Person } from '../interfaces/people';
import * as moment from 'moment';

import { IUserRequest, IUserResponse } from "../interfaces/express";


import { Common } from '../helpers/common';
import {SqlStore} from '../db/db';

export function PeopleController (db: SqlStore) {
	const router: Router = Router();

	router.get('/get', (req: IUserRequest, res: Response) => {
		let accountID = req.user.account_id;

		db.getPeople(accountID)
		.then(result => {

			result.data = result.data.map(ele => {
				ele.department = Common.createDataItem(ele.department, ele.department_name);
				ele.position = Common.createDataItem(ele.position, ele.position_name);
				ele.shift = Common.createDataItem(ele.shift, ele.shift_name);
				ele.location = Common.createDataItem(ele.location, ele.location_name);
				return ele;
			});

			res.status(200).send(result.data)
		})
		.catch((err) => {
			console.trace(err);
		});
	});

	router.get('/get/names', (req: IUserRequest, res: Response) => {
		const accountId = req.user.account_id;
		db.getPeopleNames(accountId)
		.then(result => {
			res.status(200).send(result.data);
		})
		.catch(err => {
			console.log(err);
		});
	});

	router.get('/get/:id', async (req: Request, res: Response) => {
		const id = req.params.id;
		
		try {
			let personInfo = await getPersonId(id);
			let userAccounts = await getUserAccountByPersonId(id);
			let stats = await getUserProfileStats(id);
			let training = await getTraining(id);

			res.status(200).send({
				person: personInfo,
				training: training,
				users:userAccounts,
				stats: stats
			});
		}
		catch (err) {
			console.log(err);
		}


		function getPersonId(id) {
			return db.getPerson(id)
			.then(result => {
				result.data = result.data[0];
				result.data.department = Common.createDataItem(result.data.department, result.data.department_name);
				result.data.position = Common.createDataItem(result.data.position, result.data.position_name);
				result.data.shift = Common.createDataItem(result.data.shift, result.data.shift_name);
				result.data.location = Common.createDataItem(result.data.location, result.data.location_name);
				return result.data;
			})
			.catch(error => console.trace(error));
		}

		function getUserAccountByPersonId(id){
			return db.getUserAccountByPersonId(id)
			.then(result => result.data)
			.catch(error => console.trace(error));
		}

		function getUserProfileStats(id) {
			return db.getUserProfileStats(id)
			.then(result => {
				let data = result.data[0];
				let total: number = 0;
				

				for (const key in data) {
					if (data.hasOwnProperty(key)) {
						total += parseInt(data[key]);
					}
				}

				data.total = total;
				return data;
			})
			.catch(error => console.trace(error));
		};
		function getTraining(id) {
			return db.getTrainingForPerson(id)
			.then(response => response.data);
		}
	});

	router.get('/archive/:id/:archived', async (req: IUserRequest, res:Response) => {
		const id = req.params.id;
		const accountId = req.user.account_id;
		let archiveState = (req.params.archived == 'true') ? false : true; 

		db.updateArchiveStatePersonById(id, accountId, archiveState)
      .then(result => res.status(200).send(archiveState))
      .catch(err => console.trace(err));

		
	});

	// Does not work. 
	// Missing validation rules
	router.post('/add', (req: IUserRequest, res: Response) => {

		let person: Person = req.body.person;

		let valid = true; // todo validation

		if(valid){

			person.department = person.department.id;
			person.position = person.position.id;
			person.shift = person.shift.id;
			person.location = person.location.id;
			person.account_id = req.user.account_id;

			db.addPerson(person)
			.then((result) => {
				res.status(200).send(result.data);
			})
			.catch(error => {
				res.status(400).send("bad");
			});
		}
	});

	router.get("/delete/:id", (req: IUserRequest, res: Response) => {
		let id = req.params.id;

		db.deletePerson(id)
      .then(result => {
        res.status(200).send(result.data);
      })
      .catch(error => {
        res.status(400).send("Failed to delete person");
      });
	})

	// Does not work. 
	// Missing validation rules
	router.post('/update/:id', (req: Request, res: Response) => {
		const id = req.params.id;
		let person = req.body.person;
		person.last_mod_by = 1; // set to req.user when auth has been implemented

		let valid = true; // todo validation

		if(valid) {
			person.department = person.department.id;
			person.position = person.position.id;
			person.shift = person.shift.id;
			person.location = person.location.id;
			
			db.updatePerson(id, person)
			.then(result => {
				res.status(201).send(result.data);
			}, (error) => {
				console.log(error);
			});
		}
	});


	// Training
	router.post('/training/add', (req: IUserRequest, res: Response) => {
		let training = req.body.training;
		
		db.addTrainingToPerson(training)
		.then(result => {
			res.status(200).send("OK");
		})
		.catch(error => {
			res.status(500).send(error.stack);
		})
	});

	router.post('/training/update', (req: IUserRequest, res: Response) => {
		let training = req.body.training;

		db.updateTrainingForPerson(training)
		.then(result => {
			res.status(200).send("Ok");
		})
		.catch(error => {
			res.status(500).send(error.stack);
		});
	});

	router.get("/training/delete/:id", (req: IUserRequest, res: Response) => {
		let id = req.params.id;

		db.deleteTraining(id)
		.then(result => {
			res.status(200).send("Ok");
		})
		.catch(error => {
			res.status(500).send(error.stack);
		});
	});


	// Absences

	router.get("/absence/get", (req: IUserRequest, res: Response) => {
		let accountId = req.user.account_id;

		db.getAbsences(accountId)
		.then(response => {
			res.status(200).send(response.data);
		})
		.catch(error => {
			console.error(error);
		});
	})

	router.post("/absence/add", (req: IUserRequest, res: Response) => {
		let absence = req.body.absence;
		absence.employee_id = absence.employee.id;
		absence.type = absence.type.id;
		absence.account_id = req.user.account_id;

		db.addAbsence(absence)
		.then(response => {
			res.status(200).send("OK");
		})
		.catch(error => {
			res.status(500).send(error.stack);
		});
	});
	

	return router;
}