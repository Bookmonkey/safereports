import * as pg from "pg";
import * as mssql from "mssql";
import * as bcrypt from "bcrypt-nodejs";

import { CONFIG } from "../config/config";

import { Person, Training } from "../interfaces/people";
import { RegisterItem, SafetyReport } from "../interfaces/safety";
import { DataItem } from "../interfaces/admin";
import { Asset } from "../interfaces/assets";
import { User } from "../interfaces/users";

/*
* Microsoft Server SQL Database Definition
*/
class MssqlDb {
  pool: any;
  constructor() {
    this.connect(CONFIG.db.mssql);
  }

  private connect(dbConfig) {
    this.pool = new mssql.ConnectionPool(dbConfig);

    this.pool.connect(err => {
      if (err != null) {
        console.error(err);
      }
      console.log("MSSQL Connection Established!");
    });
  }

  private queryResults(sql, params) {
    return new Promise((resolve, reject) => {
      this.expandParams(new mssql.Request(this.pool), params)
        .query(sql)
        .then(result => {
          return {
            data: result.recordsets,
            error: false
          };
        })
        .catch(err => {
          console.error(err);
        });
    });
  }

  expandParams(request, array) {
    for (let i = 0; i < array.length; i++) {
      request.input(i + 1, array[i]);
    }
    return request;
  }

  private transformJSONObject(object) {
    return `'${object}'`;
  }
}

/*
* Postgres Database Definition
*/
class PgDB {
  pool: any;
  constructor() {
    this.connect(CONFIG.db.pg);
  }

  private connect(dbConfig) {
    this.pool = new pg.Pool(dbConfig);

    this.pool.on("error", function(err, client) {
      console.error("idle client error", err.message, err.stack);
    });
  }

  private queryResults(sql, params) {
    return new Promise((resolve, reject) => {
      this.pool
        .query(sql, params)
        .then(res => {
          resolve({
            data: res.rows,
            error: false
          });
        })
        .catch(error => {
          console.error(error.stack);
          console.error(sql);
          console.error(params);
          reject({
            data: error.stack,
            error: true,
            sql: sql,
            params: params
          });
        });
    });
  }
}

/*
* SqlStore
*/
export abstract class SqlStore {
  protected db: any;
  constructor(db) {
    this.db = db;
  }
  protected abstract sqlParams(parts: TemplateStringsArray, ...values);

  private linkDataItemById(id: Number, name) {
    return `(SELECT name FROM data_collection_item where id = ${id}) as ${name}`;
  }

  /**
   * Returns results from a database connection
   * @param sql
   * @param params
   */
  queryResults(sql: string, params: any[]): Promise<any> {
    return this.db.queryResults(sql, params);
  }

  /** ----------------------------------
   * People
   -----------------------------------*/
  public getPeople(accountID: number) {
    const { sql, params } = this
      .sqlParams`select *, concat(first_name, ' ', last_name) as full_name,
        (SELECT name FROM data_collection_item where id = department) as department_name,
        (SELECT name FROM data_collection_item where id = position) as position_name,
        (SELECT name FROM data_collection_item where id = shift) as shift_name,
        (SELECT name FROM data_collection_item where id = location) as location_name
        from person
        where account_id = ${accountID}
        order by full_name`;
    return this.queryResults(sql, params);
  }
  public getPerson(id: Number) {
    const { sql, params } = this.sqlParams`
        select *, concat(first_name, ' ', last_name) as full_name,
        (SELECT name FROM data_collection_item where id = department) as department_name,
        (SELECT name FROM data_collection_item where id = position) as position_name,
        (SELECT name FROM data_collection_item where id = shift) as shift_name,
        (SELECT name FROM data_collection_item where id = location) as location_name
        from person where id = ${id};`;
    return this.queryResults(sql, params);
  }

  public getUserAccountByPersonId(id: Number) {
    const { sql, params } = this
      .sqlParams`select username, id from user_account where person_id = ${id};`;
    return this.queryResults(sql, params);
  }

  public getPeopleNames(accountId) {
    const { sql, params } = this
      .sqlParams`select id, concat(first_name, ' ', last_name) as name from person where account_id = ${accountId};`;
    return this.queryResults(sql, params);
  }
  public addPerson(person: Person) {
    console.log(person);
    const { sql, params } = this.sqlParams`
            insert into person (first_name, last_name, birthdate, gender, department, position, shift, location, general_notes, special_notes, phone, email, archived, account_id) 
            VALUES (
                ${person.first_name},
                ${person.last_name},
                ${person.birthdate},
                ${person.gender},
                ${person.department},
                ${person.position},
                ${person.shift},
                ${person.location},
                ${person.general_notes},
                ${person.special_notes},
                ${person.phone},
                ${person.email},
                false,
                ${person.account_id}
            ) returning id;
        `;
    return this.queryResults(sql, params);
  }
  public updatePerson(id: Number, person: Person) {
    const { sql, params } = this.sqlParams`
            update person set 
                first_name = ${person.first_name},
                last_name = ${person.last_name},
                birthdate = ${person.birthdate},
                gender = ${person.gender},
                department = ${person.department},
                position = ${person.position},
                shift = ${person.shift},
                location = ${person.location},
                general_notes = ${person.general_notes},
                special_notes = ${person.special_notes},
                phone = ${person.phone},
                email = ${person.email}
            where id = ${id} returning id;`;

    return this.queryResults(sql, params);
  }

  public updateArchiveStatePersonById(
    id: Number,
    accountID: Number,
    archiveState: boolean
  ) {
    const { sql, params } = this.sqlParams`
        update person set archived = ${archiveState} where id = ${id} and account_id = ${accountID};
    `;
    return this.queryResults(sql, params);
  }

  public deletePerson(personId: number) {
    const { sql, params } = this.sqlParams`
      delete from person where id = ${personId};
    `;
    return this.queryResults(sql, params);
  }

  public getTrainingForPerson(personId: number) {
    const { sql, params } = this.sqlParams`
      select *, 
      (SELECT name FROM data_collection_item where training_id = id) as training_name 
      from training where person_id = ${personId};
    `;

    return this.queryResults(sql, params);
  }


  public addTrainingToPerson(training: Training) {
    const { sql, params } = this.sqlParams`
      insert into training (training_id, acquired_on, expires_on, notes, person_id)
      values(${training.training_id }, ${training.acquired_on}, ${training.expires_on}, ${training.notes}, ${training.person_id});
    `;

    return this.queryResults(sql, params);
  }

  public updateTrainingForPerson(training: Training) {
    const { sql, params } = this.sqlParams `
      update training 
        set training_id = ${training.training_id},
        acquired_on = ${training.acquired_on},
        expires_on = ${training.expires_on},
        notes = ${training.notes}
        where id = ${training.id};
    `;

    return this.queryResults(sql, params);
  };

  public deleteTraining(trainingId: number) {
    const { sql, params } = this.sqlParams`
      delete from training where id = ${trainingId};
    `;
    return this.queryResults(sql, params);
  };


  /** ----------------------------------
   * Absences
   -----------------------------------*/
   public getAbsences(accountId: number) {
     const { sql, params } = this.sqlParams`
      select * from absence where account_id = ${accountId};
     `
     return this.queryResults(sql, params);
   };

   public addAbsence(absence: any) {
    const { sql, params } = this.sqlParams`
      insert into absence (
        absence_date,
        employee_id,
        type,
        expected_return_date,
        reason,
        account_id
      ) 
      VALUES(
        ${ absence.absence_date },
        ${ absence.employee_id },
        ${ absence.type },
        ${ absence.expected_return_date },
        ${ absence.reason },
        ${ absence.account_id }
      );
    `;
    return this.queryResults(sql, params);
   }; 
  
  /** ----------------------------------
   * Safety
   -----------------------------------*/
  public getSafetyReports(accountID) {
    const { sql, params } = this.sqlParams`
        select *,
            (SELECT concat(first_name, ' ', last_name) from person where id = safety_report.employee_id) as employee,
            (SELECT concat(first_name, ' ', last_name) from person where id = safety_report.supervisor_id) as supervisor,
            (SELECT name FROM data_collection_item where id = location) as location_name,
            (SELECT name FROM data_collection_item where id = department) as department_name
        from safety_report
        where account_id = ${accountID}
        order by date_occurred desc
        limit 100;`;
    return this.queryResults(sql, params);
  }
  public getSafetyReportById(id: Number) {
    const { sql, params } = this.sqlParams`
            select *,
            (SELECT concat(first_name, ' ', last_name) from person where id = safety_report.employee_id) as employee,
            (SELECT concat(first_name, ' ', last_name) from person where id = safety_report.supervisor_id) as supervisor,
            (SELECT name FROM data_collection_item where id = location) as location_name,
            (SELECT name FROM data_collection_item where id = department) as department_name
            from safety_report where id = ${id};`;
    return this.queryResults(sql, params);
  }
  public addSafetyReport(safetyReport: SafetyReport) {
    const { sql, params } = this.sqlParams`
        insert into safety_report (
            type,
            date_occurred,
            location,
            department,
            what_happened,
            employee_id,
            supervisor_id,
            damage_description,
            damages,
            harm_description,
            harm,
            risk_likelihood,
            risk_harm,
            corrective_actions,
            issue_still_present,
            account_id
        ) 
        VALUES (
            ${safetyReport.type},
            ${safetyReport.date_occurred},
            ${safetyReport.location},
            ${safetyReport.department},
            ${safetyReport.what_happened},
            ${safetyReport.employee_id},
            ${safetyReport.supervisor_id},
            ${safetyReport.damage_description},
            ${safetyReport.damages},
            ${safetyReport.harm_description},
            ${safetyReport.harm},
            ${safetyReport.risk_likelihood},
            ${safetyReport.risk_harm},
            ${safetyReport.corrective_actions},
            ${safetyReport.issue_still_present},
            ${safetyReport.account_id}
        ) returning id;`;

    return this.queryResults(sql, params);
  }
  
  public updateSafetyReport(safetyReport: SafetyReport) {
    const { sql, params } = this.sqlParams`
            update safety_report set
            type = ${safetyReport.type},
            date_occurred = ${safetyReport.date_occurred},
            location = ${safetyReport.location},
            department = ${safetyReport.department},
            what_happened = ${safetyReport.what_happened},
            employee_id = ${safetyReport.employee_id},
            supervisor_id = ${safetyReport.supervisor_id},
            damage_description = ${safetyReport.damage_description},
            damages = ${safetyReport.damages},
            harm_description = ${safetyReport.harm_description},
            harm = ${safetyReport.harm},
            risk_likelihood = ${safetyReport.risk_likelihood},
            risk_harm = ${safetyReport.risk_harm},
            corrective_actions = ${safetyReport.corrective_actions},
            issue_still_present = ${safetyReport.issue_still_present},
            account_id = ${safetyReport.account_id}
            where id = ${safetyReport.id};
        `;
    return this.queryResults(sql, params);
  }
  
  public signOffSafetyReport(id: Number, signOff: Object) {
    const { sql, params } = this.sqlParams``;
    return this.queryResults(sql, params);
  }
  
  
  // Safety Investigations

  public addIncidentInvestigation(investigation: any) {
    investigation.factors = `{ ${investigation.factors} }`;
    const { sql, params } = this.sqlParams`
      insert into safety_report_investigation (
        findings,
        recommendations,
        factors,
        safety_report_id
      )
      VALUES (
        ${investigation.findings},
        ${investigation.recommendations},
        ${investigation.factors},
        ${investigation.id}
      );
    `;

    return this.queryResults(sql, params);
  }

  public updateIncidentInvestigation(investigation: any) {
    investigation.factors = `{ ${investigation.factors} }`;
    const { sql, params } = this.sqlParams`
      update safety_report_investigation 
      set findings = ${investigation.findings},
      recommendations = ${investigation.recommendations},
      factors = ${investigation.factors}
      where id = ${investigation.id};
    `;

    return this.queryResults(sql, params);
  };

  public getSafetyReportInvestigation(id: number) {
    console.log(id);
    const { sql, params } = this.sqlParams`
      select * from safety_report_investigation where safety_report_id = ${id};
    `;
    return this.queryResults(sql, params);
  }

  
  public getHazardLinksOnSafetyReport(id) {
    const { sql, params } = this.sqlParams`
      select *, (SELECT hazard FROM hazard_register_item where id = hazard_register_item_id) as hazard from safety_report_hazard_link where safety_report_id = ${id};
    `;

    return this.queryResults(sql, params);
  } 

  public addHazardLinkToSafetyReport (hazardLinks, userID){
    let safetyReportID = hazardLinks.safetyReportID;
    let hazardIds = hazardLinks.hazardIds;

    let sql = 'insert into safety_report_hazard_link (safety_report_id, hazard_register_item_id, created_by, created_on) VALUES ';
    let params = [];
    let incrementer = 1;

    hazardIds.map(ele => {
      if(incrementer > 1) {
        sql += ", ";
      }

      sql += `($${incrementer++}, $${incrementer++}, $${incrementer++}, $${incrementer++})`
  
      params.push(safetyReportID, ele, userID, new Date());
    });

    sql+= ";"
    return this.queryResults(sql, params);
  };

  public getHazardRegisters(accountId: Number) {
    const { sql, params } = this.sqlParams`
        select *,
          (SELECT name FROM data_collection_item where id = location) as location_name
        from hazard_register_item
        where account_id = ${accountId}
        order by date_reported desc`;
    return this.queryResults(sql, params);
  }

  public removeHazardLinkFromIncidentReport(linkID) {
    const { sql, params } = this.sqlParams`delete from safety_report_hazard_link where id = ${linkID}`;
    return this.queryResults(sql, params);
  }


  public getIncidentsLinkedOnHazard(hazardID) {
    const { sql, params } = this.sqlParams`
      select 
        link.*, 
        safety.date_occurred,
        (SELECT name FROM data_collection_item where id = location) as location_name,
        (SELECT name FROM data_collection_item where id = department) as department_name
      from safety_report_hazard_link as link
      left join safety_report as safety on safety.id = link.safety_report_id
      where link.hazard_register_item_id = ${hazardID}
    `;
    return this.queryResults(sql, params);
  }

  public getHazardRegistersById(id: Number) {
    const { sql, params } = this.sqlParams`select *,
        (SELECT name FROM data_collection_item where id = location) as location_name from hazard_register_item where id= ${id};`;
    return this.queryResults(sql, params);
  }

  public getHazardRegistersByLocationId(locationId: Number) {
    const { sql, params } = this.sqlParams`select *,
        (SELECT name FROM data_collection_item where id = location) as location_name from hazard_register_item where location_id = ${locationId};`;
    return this.queryResults(sql, params);
  }

  public addHazardRegisterItem(item: RegisterItem) {
    const { sql, params } = this.sqlParams`
            insert into 
            hazard_register_item (hazard, potential_harm, hazard_mitigation, controls, risk_factor, location, review_date, date_reported, archived, account_id)
            VALUES (
                ${item.hazard},
                ${item.potential_harm},
                ${item.hazard_mitigation},
                ${item.controls},
                ${item.risk_factor},
                ${item.location},
                ${item.review_date},
                ${new Date()}, 
                false,
                ${item.account_id}
            ) returning id;`;
    // console.log(sql);
    return this.queryResults(sql, params);
  }

  public updateHazardRegisterItem(item: RegisterItem) {
    const { sql, params } = this.sqlParams`
            update hazard_register_item set
                hazard = ${item.hazard},
                potential_harm = ${item.potential_harm},
                hazard_mitigation = ${item.hazard_mitigation},
                controls = ${item.controls},
                risk_factor = ${item.risk_factor},
                review_date = ${item.review_date},
                location = ${item.location}
            where id = ${item.id};`;
    return this.queryResults(sql, params);
  }

  public archiveRequestHazardRegisterItem(id, state) {
    const { sql, params } = this
      .sqlParams`update hazard_register_item set archived = ${state} where id = ${id};`;
    return this.queryResults(sql, params);
  }

  public unArchiveHazardRegisterItem(id) {
    const { sql, params } = this
      .sqlParams`update hazard_register_item set archived = false where id = ${id};`;
    return this.queryResults(sql, params);
  }

  /** ----------------------------------
   * Assets
   *-----------------------------------*/
  public getAssets(accountId) {
    const { sql, params } = this.sqlParams`
    select *,
    (SELECT name FROM data_collection_item where id = location) as location_name
    from asset
    where account_id = ${accountId};
  `;
    return this.queryResults(sql, params);
  }

  public getAssetById(id: Number) {
    const { sql, params } = this.sqlParams`
    select *,
    (SELECT name FROM data_collection_item where id = location) as location_name
    from asset
    where id = ${id};
  `;

    return this.queryResults(sql, params);
  }

  public addAsset(asset: Asset) {
    const { sql, params } = this.sqlParams`
    insert into asset (
      type, 
      asset_id, 
      location, 
      comments,
      manfacture_date,
      wof_date,
      cof_date,
      next_service_date
    ) 
    VALUES(
      ${asset.type},
      ${asset.asset_id},
      ${asset.location},
      ${asset.comments},
      ${asset.manfacture_date},
      ${asset.wof_date},
      ${asset.cof_date},
      ${asset.next_service_date}
    ) returning id;`;

    return this.queryResults(sql, params);
  }

  public updateAsset(asset: Asset) {
    const { sql, params } = this.sqlParams`
    update asset set 
      type = ${asset.type},
      asset_id = ${asset.asset_id},
      location = ${asset.location},
      comments = ${asset.comments},
      manfacture_date = ${asset.manfacture_date},
      wof_date = ${asset.wof_date},
      cof_date = ${asset.cof_date},
      next_service_date = ${asset.next_service_date}
    where id = ${asset.id};
  `;
    return this.queryResults(sql, params);
  }

  public deleteAssetById(id: Number) {
    const { sql, params } = this.sqlParams`
    delete from asset where id = ${id};
  `;

    return this.queryResults(sql, params);
  }


  public getServiceLogsById(id: number) {
    const { sql, params } = this.sqlParams`
      select 
        *,
        (SELECT name FROM data_collection_item where id = service_type) as service_name
      from asset_service_log
      where asset_id = ${id};
    `;

    return this.queryResults(sql, params);
  }

  public addServiceLogToAsset(service: any) {
    const { sql, params } = this.sqlParams`
      insert into asset_service_log (
        service_type,
        service_date,
        service_comments,
        asset_id,
        created_by,
        account_id
      )
      VALUES (
        ${service.service_type},
        ${service.service_date},
        ${service.service_comments},
        ${service.asset_id},
        ${service.created_by},
        ${service.account_id}
      ) returning *;
    `;
    return this.queryResults(sql, params);
  }

  /** ----------------------------------
   * Suppliers
   -----------------------------------*/

   public getSuppliers(accountId: number) {
    const { sql, params } = this.sqlParams`
      select * from supplier where account_id = ${accountId};
    `;

    return this.queryResults(sql, params);
   };

   public getSupplierById(supplierId: number, accountId: number) {
     const { sql, params } = this.sqlParams`
      select * from supplier where id = ${supplierId} AND account_id = ${accountId}; 
    `;

    return this.queryResults(sql, params);
   };

   public addSupplier(supplier: any) {
    const { sql, params } = this.sqlParams``;
    return this.queryResults(sql, params);
   };

   public updateSupplier(supplier: any) {
     const { sql, params } = this.sqlParams``;
     return this.queryResults(sql, params);
   }

   public deleteSupplier(supplierId: number, accountId: number) {
     const { sql, params } = this.sqlParams`
      delete from supplier where id = ${supplierId} and account_id = ${accountId}
     `;
     return this.queryResults(sql, params);
   }

  // Documents
  public getDocuments() {
    const { sql, params } = this.sqlParams`select * from documents limt 100;`;
    return this.queryResults(sql, params);
  }
  public getDocumentById(id: Number) {
    const { sql, params } = this
      .sqlParams`select * from documents where id = ${id};`;
    return this.queryResults(sql, params);
  }
  public addDocument(document: Object) {
    const { sql, params } = this
      .sqlParams`insert into documents into (document_id, document_name, ...) VALUES (...);`;
    return this.queryResults(sql, params);
  }

  // Admin

  // datasets
  public getSettingsDataCollections() {}
  public getDataCollectionsGroups() {
    const { sql, params } = this
      .sqlParams`select * from data_collection_group;`;

    return this.queryResults(sql, params);
  }

  public getDataCollectionsByGroupId(id: Number, accountID) {
    const { sql, params } = this
      .sqlParams`select * from data_collection_item where account_id = ${accountID} AND grouping_id = ${id};`;

    return this.queryResults(sql, params);
  }

  public getDataCollectionAndItems(
    dataCollections: Array<String>,
    accountID: Number
  ) {
    const statement = {
      sql: `select * from data_collection_group 
            join data_collection_item on data_collection_group.id = data_collection_item .grouping_id
            where key in (${dataCollections.map(
              (ele, index) => `$${index + 1}`
            )}) and account_id = ${accountID};`,
      params: dataCollections
    };
    return this.queryResults(statement.sql, statement.params);
  }

  public addDataItemToCollection(
    collectionId: Number,
    newItem: String,
    accountID: Number
  ) {
    const { sql, params } = this
      .sqlParams`insert into data_collection_item(name, account_id, grouping_id)
        VALUES (
            ${newItem},
            ${accountID},
            ${collectionId}
        ) returning *;`;
    return this.queryResults(sql, params);
  }
  public updateDataItem(item: DataItem) {
    const { sql, params } = this.sqlParams`update data_collection_item 
        set name = ${item.name},
        grouping_id = ${item.grouping_id}
        where id = ${item.id}`;
    return this.queryResults(sql, params);
  }
  public deleteDataItem(itemID: Number, accountID: Number) {
    const { sql, params } = this
      .sqlParams`delete from data_collection_item where id = ${itemID} and account_id = ${accountID};`;
    return this.queryResults(sql, params);
  }

  // Settings

  public getUserSettingsModules() {
    const { sql, params } = this.sqlParams`select * from settings_modules where active = true;`;
    return this.queryResults(sql, params);
  }

  // Users
  public getUsers(accountID: Number) {
    const { sql, params } = this.sqlParams`
        select id, username, mobile_phone, person_id, account_id,
            (SELECT concat(first_name, ' ', last_name) from person where id = user_account.person_id) as name
        from user_account
        where account_id = ${accountID}
        order by username;`;
    return this.queryResults(sql, params);
  }
  public getUserById(id: Number) {
    const { sql, params } = this.sqlParams`
        select 
            id,
            username,
            mobile_phone, 
            person_id,
            is_developer,
            account_id
        from user_account where id = ${id};`;
    return this.queryResults(sql, params);
  }

  public getUserModulesById(userId: Number) {
    const { sql, params } = this.sqlParams`
        select 
            user_modules.id,
            user_modules.module_id,
            settings_modules.module_name,
            settings_modules.module_key,
            settings_modules.active 
        from user_modules
        left join settings_modules on user_modules.module_id = settings_modules.id
        where user_id =  ${userId};
        `;
    return this.queryResults(sql, params);
  }

  public getUserByUsername(username: String) {
    const { sql, params } = this
      .sqlParams`select * from user_account where username = ${username};`;
    return this.queryResults(sql, params);
  }

  public addUser(user: User) {
    user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(8));
    const { sql, params } = this.sqlParams`
        insert into user_account 
        (username, mobile_phone, password, person_id, account_id)
        VALUES (
            ${user.email},
            ${user.mobile_number},
            ${user.password},
            ${user.person},
            ${user.account_id}
        ) returning id;`;

    return this.queryResults(sql, params);
  }

  public updateUser(user: User) {
    const { sql, params } = this.sqlParams`
        update user_account set 
        username = ${user.username},
        mobile_phone = ${user.mobile_phone}
        where id = ${user.id};`;
    return this.queryResults(sql, params);
  }

  public addModulesToUser(userId: number, modules: any) {
    let index = 1;
    let params = [];
    let sql = `insert into user_modules (user_id, module_id) VALUES `;
    sql += modules
      .map(ele => {
        index = index + 2;
        params.push(userId);
        params.push(ele);
        return `($${index - 2}, $${index - 1})`;
      })
      .join(",");
    sql += ";";
    return this.queryResults(sql, params);
  }

  public addeModuleToUser(userId: number, moduleId: number) {
    const { sql, params } = this
      .sqlParams`insert into user_modules (user_id, module_id) VALUES (${userId}, ${moduleId}) returning *;`;
    return this.queryResults(sql, params);
  }

  public addAllModulesToUser(userId:number) {
    const { sql, params } = this.sqlParams`
    INSERT INTO user_modules (id, col_1, col_2, col_3)
    SELECT id, 'data1', 'data2', 'data3'
    FROM TABLE2
    WHERE col_a = 'something';`;
  }

  public removeModuleFromUser(userId: number, moduleId: number) {
    const { sql, params } = this
      .sqlParams`delete from user_modules where user_id = ${userId} and module_id = ${moduleId};`;
    return this.queryResults(sql, params);
  }

  public deleteModulesOnUserID(userId: number) {
    const { sql, params } = this
      .sqlParams`delete from user_modules where user_id = ${userId};`;
    return this.queryResults(sql, params);
  }

  public deleteUserById(userId: number) {
    const { sql, params } = this
      .sqlParams`delete from user_account where id = ${userId};`;
    return this.queryResults(sql, params);
  }

  // business accounts
  public createBusinessAccount(planID) {

    const { sql, params } = this.sqlParams`
      insert into business_account (active, plan_id)
      VALUES(true, ${planID})
      returning id;
    `;

    return this.queryResults(sql, params);
  }
  public createBusinessContact(form, accountId) {

    const { sql, params } = this.sqlParams`
      insert into business_account_contact (account_id, name, contact_person, email, phone)
      VALUES(${accountId}, ${form.businessName}, ${form.businessContact}, ${form.businessEmail}, ${form.businessPhone})
      returning id;
    `;

    return this.queryResults(sql, params);
  }

  public getBusinessAccountDetails(accountId) {
    const { sql, params } = this.sqlParams`
            select * from business_account as account
            left join business_account_contact as contact on contact.account_id = account.id
            left join settings_plan_details as plan on plan.id = account.plan_id
            where account.id = ${accountId};
        `;
    return this.queryResults(sql, params);
  }

  public updateBusinessAccountDetails(account) {
    const { sql, params } = this.sqlParams`
      update business_account_contact set 
      name = ${account.name},
      contact_person = ${account.contact_person},
      email = ${account.email},
      phone = ${account.phone} 
      where id = ${account.id};
    `;
    return this.queryResults(sql, params);
  }
  /** ----------------------------------
   * Dashboard
   -----------------------------------*/
  public getDashboardStatsForSafety(period) {
    const sql = `
      SELECT 
        COUNT(*), 
        extract('month' from date_occurred) as month,
	      TO_CHAR(date_occurred, 'YY') as year
      FROM safety_report where date_occurred >= current_date - interval '${period}' 
      AND date_occurred <= current_date + interval '1 day'
      group by month, year
      order by year asc, month;
    `;
    return this.queryResults(sql, null);
  }

  public getDashboardStatsForHazards(period) {
    const sql = `
      SELECT
        COUNT(*),
        extract('month' from date_reported) as month,
        TO_CHAR(date_reported, 'YY') as year
      FROM hazard_register_item where date_reported >=  current_date - interval '${period}' 
      AND date_reported <= current_date + interval '1 day'
      group by month, year
      order by year asc, month;
    `;
    return this.queryResults(sql, null);
  }

  public getSummaryNotifications() {

    function whereStatement(date) {
      return `
        WHERE (${date} < current_date + interval '1 month' 
        OR ${date} < current_date)
        AND ${date} > current_date - interval '1 month'
      `;
    }
    

    const sql = `
      SELECT 
      (SELECT COUNT(*) FROM asset ${whereStatement("wof_date")}) as wof_due,
      (SELECT COUNT(*) FROM asset ${whereStatement("cof_date")}) as cof_due,
      (SELECT COUNT(*) FROM asset ${whereStatement("next_service_date")}) as service_due,
      (SELECT COUNT(*) FROM hazard_register_item ${whereStatement("review_date")}) as hazard_review
    `;

    return this.queryResults(sql, null);
  }

  public getNotifications() {
    const { sql, params } = this.sqlParams`
      SELECT id, type as secondary, asset_id as title, wof_date as due_date, 'WOF' as reason, 'Asset' as type  FROM asset 
      WHERE (wof_date < current_date + interval '1 month' 
      OR wof_date < current_date)
      AND wof_date > current_date - interval '1 month'
     
      UNION ALL

      SELECT id, type as secondary, asset_id as title, cof_date as due_date, 'COF' as reason, 'Asset' as type FROM asset 
      WHERE (cof_date < current_date + interval '1 month' 
      OR cof_date < current_date)
      AND cof_date > current_date - interval '1 month'

      UNION ALL

      SELECT id, type as secondary, asset_id as title, next_service_date as due_date, 'Service' as reason, 'Asset' as type FROM asset 
      WHERE (next_service_date < current_date + interval '1 month' 
      OR next_service_date < current_date)
      AND next_service_date > current_date - interval '1 month'

      UNION ALL

      SELECT hri.id, dci.name as secondary, hri.hazard as title, hri.review_date as due_date, 'review' as reason, 'Hazard' as type 
      FROM hazard_register_item as hri
      left join data_collection_item as dci on dci.id = location
      WHERE (review_date < current_date + interval '1 month' 
      OR review_date < current_date)
      AND review_date > current_date - interval '1 month'
      
      order by due_date;
    `;

    return this.queryResults(sql, params);
  }

  public getUserProfileStats(userId: Number) {
    const { sql, params } = this.sqlParams`
      SELECT 
        (SELECT COUNT(*) FROM safety_report where supervisor_id = ${userId}) as supervisor,
        (SELECT COUNT(*) FROM safety_report where employee_id = ${userId}) as employee;
    `;

    return this.queryResults(sql, params);
  }

  public getHazardRegisterStats(hazardRegisterId) {
    const { sql, params } = this.sqlParams``;
  }


  /** ----------------------------------
  * Draft Mode
  *-----------------------------------*/

  public getDraftsForUser(id, location) {
    const { sql, params } = this.sqlParams`
      select * from draft where user_id = ${id} AND location = ${location};
    `;
    return this.queryResults(sql, params);
  }

  public getDraftById(draftId, userId) {
    const { sql, params } = this.sqlParams`
      select * from draft where user_id = ${userId} AND id = ${draftId};
    `;
    return this.queryResults(sql, params);
  }

  public addDraft(draft) {
    const { sql, params } = this.sqlParams`
      insert into draft (location, document, user_id) 
      VALUES (${draft.location}, ${draft.document}, ${draft.user_id});
    `;

    return this.queryResults(sql, params);
  }

  public updateDraft(draft) {
    const { sql, params } = this.sqlParams`
      update draft set
      location = ${draft.location},
      document = ${draft.document}
      where id = ${draft.id};
    `;

    return this.queryResults(sql, params);
  }

  public deleteDraftById(id) {
    const { sql, params } = this.sqlParams`
      delete from draft where id = ${id};
    `;
    return this.queryResults(sql, params);
  }

  
}

/*
* PgStore
*/
export class PgStore extends SqlStore {
  constructor() {
    super(new PgDB());
  }

  protected sqlParams(parts: TemplateStringsArray, ...values) {
    return {
      sql: parts.reduce((prev, curr, i) => prev + "$" + i + curr),
      params: values
    };
  }
}

/*
* MssqlStore
*/
export class MssqlStore extends SqlStore {
  constructor() {
    super(new MssqlDb());
  }

  protected sqlParams(parts: TemplateStringsArray, ...values) {
    return {
      sql: parts.reduce((prev, curr, i) => prev + "@" + i + curr),
      params: values
    };
  }
}
