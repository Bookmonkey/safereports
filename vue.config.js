module.exports = {
  lintOnSave: false,

  devServer: {
    open: process.platform === "darwin",
    host: "localhost",
    port: 3000,
    https: false,
    hotOnly: false,
    proxy: {
      "/api": {
        target: "http://localhost:8080",
        ws: true,
        changeOrigin: true
      }
    } // string | Object
  }
};