# Documentation
## Tech Stack
```
Vue2 with Typescript
tbc on backend
```
## Folder Structure
Each module will have its own folder with respected pages inside that folder. For example the People module will look something like:

```
-src
    components
        people
            add.vue
            view-all.vue
            view-person.vue
        oltherfolders..
```

### File convention
As mentioned above you each module will have its own directory so do not name files like `people-add.vue` as it will be under the people directory. Try to use singular terms. 

## Browser support
```
IE 10+ 
Edge
Google Chrome
Firefox
Safari
```

## Styling (?)
For the time being the we will be writing our own CSS, until Bootstrap 4 is `stable`.
