## Modal Component

## Configuration
```
options: {
    shown: boolean,
    size: string,
    actions:{
        closeEvent: boolean,
        dismissEvent: boolean,
    }
};
```

### HTML Markup
```
<modal v-if="modalOptions.shown" @close="close()" :options="modalOptions">   
    <div slot="header">custom header</div>
    <div slot="body"></div>
    <div slot="footer"></div>
</modal>
```
`Slot` override the markup. Look at Vue Components for more detail on this.


### Close / dismiss events
Close is considered the `success` event, while the dismiss event is the `cancel` event


To handle something outside of the modal component after it has closed / dismissed you need to do two things:
 1. In the ModalObject set the closeEvent / dismissEvent to true. `options.actions.abcEvent = true`
 2. On the Modal HTML add the @close or @dismiss with the function you want to run.


 ### Sizes
- small
- normal
- large