create table settings_modules (
    id serial not null PRIMARY KEY,
    module_name text not null,
    module_key text not null,
    active boolean not null
);

create table settings_plan_details (
    id serial not null PRIMARY KEY,
    plan_name text not null,
    user_limit int not null,
    plan_cost_monthly numeric not null,
    plan_cost_annually numeric not null,
    plan_active boolean not null
);

create table business_account_contact (
    id serial not null PRIMARY KEY,
    name text not null, -- business name
    contact_person text not null,
    email text not null,
    phone text not null
    account_id int not null references business_account(id),
);
create table business_account (
    id serial not null PRIMARY KEY,
    active boolean not null,
    plan_id int not null references settings_plan_details(id)
);

create table data_collection_group (
    id serial not null PRIMARY KEY,
    name text not null,
    key text not null,
    grouping_id int references data_collection_group(id)
);

create table data_collection_item(
    id serial not null PRIMARY KEY,
    name text not null,
    account_id int not null references business_account(id),
    grouping_id int not null references data_collection_group(id),
    sub_grouping_id int references data_collection_group(id)
);

create table person (
    id serial not null PRIMARY KEY,
    first_name text not null,
    last_name text not null,
    birthdate timestamp with time zone,
    gender text,
    department int not null,
    position int,
    shift int,
    location int,
    general_notes text,
    special_notes text,
    phone text,
    email text,
    archived boolean not null,
    account_id int not null references business_account(id)    
);
create table training (
    id serial not null PRIMARY KEY,
    expires_on timestamp,
    acquired_on timestamp,
    notes text,
    training_id int not null references data_collection_item(id),
    person_id int not null references person(id)
);

create table absence (
    id serial not null primary key,
    absence_date timestamp with time zone not null,
    type int not null references data_collection_item(id),
    employee_id int not null references data_collection_item(id),
    expected_return_date timestamp with time zone not null,
    reason text,
    account_id int not null references business_account(id)
);

create table user_account (
    id serial not null PRIMARY KEY,
    username text not null,
    mobile_phone text,
    person_id int not null references person(id),
    password text not null,
    is_developer boolean,
    account_id int not null
);

create table user_modules (
    id serial not null PRIMARY KEY,
    module_id int not null references settings_modules(id),
    user_id int not null references user_account(id)
);

create table doc (
    id serial not null PRIMARY KEY,
    name text not null,
    description text,
    owner_id int not null references user_account(id),
    creation_date timestamp with time zone not null,
    review_date timestamp with time zone not null,
    modifed_date timestamp with time zone not null,
    is_draft boolean not null,
    status text not null,
    notes text not null,
    review_frequency timestamp with time zone not null,
    account_id int not null references business_account(id),
    created_by_person_id int not null references user_account(id),
    reference_type text not null,
    reference_link_id int not null
);


create table file (
    id serial not null PRIMARY KEY,
    name text not null,
    path text not null,
    type text not null,
    uploaded boolean not null
);


create table doc_change_requests(
    id serial not null PRIMARY KEY,
    details text not null,
    reason text not null,
    user_id int not null references user_account(id),
    doc_id int not null references doc(id),
    file_id int not null references file(id)
);

create table doc_approvals(
    id serial not null PRIMARY KEY,
    reason text not null, 
    approved boolean not null,
    change_request_id int not null references doc_change_requests(id),
    user_id int not null references user_account(id)
);

create table doc_links(
    id serial not null PRIMARY KEY,
    primary_doc_id int not null references doc(id),
    secondary_doc_id int not null references doc(id),
    notes text not null
);

create table doc_revision_history(
    id serial not null PRIMARY KEY,
    revision_date timestamp with time zone not null,
    revision_no int not null,
    file_id int not null references file(id),
    doc_id int not null references doc(id)
);


create table doc_approvers (
    id serial not null PRIMARY KEY,
    doc_id int not null references doc(id)
);

create table safety_report (
    id serial not null PRIMARY KEY,
    type text[],
    date_occurred timestamp with time zone not null,
    location int not null references data_collection_item(id),
    employee_id int not null references person(id),
    supervisor_id int not null references person(id),
    department int not null references data_collection_item(id),
    what_happened text not null,
    damages text[],
    damage_description text,
    harm text[],
    harm_description text,
    risk_likelihood text,
    risk_harm text,
    corrective_actions text,
    issue_still_present boolean,
    account_id int not null references data_collection_item(id)
);

create table safety_report_sign_off (
    id serial not null PRIMARY KEY,
    supervisor_sign_off_date timestamp with time zone,
    supervisor_id int not null references user_account(id),
    safety_rep_sign_off_date timestamp with time zone,
    safety_rep_id int not null references user_account(id)
);

create table safety_report_investigation (
    id serial not null PRIMARY KEY,
    findings text not null,
    recommendations text not null,
    factors text[] not null,
    safety_report_id int not null references safety_report(id)
);


create table hazard_register_item (
    id serial not null PRIMARY KEY,
    location int not null references data_collection_item(id),
    hazard text not null,
    potential_harm text not null,
    hazard_mitigation text not null,
    risk_factor text,
    controls text not null,
    document_id int references doc(id),
    date_reported timestamp with time zone,
    archived boolean not null,

    account_id int not null references business_account(id)
);

create table safety_report_hazard_link (
    id serial not null PRIMARY KEY, 
    safety_report_id int not null references safety_report(id),
    hazard_register_item_id int not null references hazard_register_item(id),
    created_by int not null references user_account(id),
    created_on timestamp with time zone not null
);


CREATE TABLE asset (
  id serial NOT NULL primary key,
  type text NOT NULL,
  asset_id text NOT NULL,
  location integer NOT NULL,
  comments text NOT NULL,
  manfacture_date timestamp without time zone,
  wof_date timestamp without time zone,
  cof_date timestamp without time zone,
  next_service_date timestamp without time zone,
  account_id int not null
);

create table asset_service_log (
  id serial not null PRIMARY KEY,
  service_type int not null,
  service_date timestamp with time zone,
  service_comments text,

  asset_id int not null references asset(id),
  created_by int not null references user_account(id),
  account_id int not null references business_account(id)
);



CREATE TABLE draft (
	id serial PRIMARY KEY not null,
	location text not null,
	document jsonb,
    user_id int not null references user_account(id)
);

-- drop table if exists person;
-- drop table if exists file;
-- drop table if exists file;
-- PostgresSQL 




-- stripe_account details
-- create table payment_info (
--     id serial not null PRIMARY KEY,
--     account_id int not null references business_business_account(id)
-- );

-- User Groups
-- create table user_group(
--     id serial not null PRIMARY KEY,
--     name text not null
-- );

-- create table user_group_members(
--     id serial not null PRIMARY KEY,
--     user_id int not null references user_account(id),
--     user_group_id int not null references user_group(id)
-- );

-- create table reference_members(
--     id serial not null PRIMARY KEY,
--     reference_type text not null,
--     reference_link_id int not null,
--     user_group_id int references user_group(id),
--     user_id int references user_account(id)
-- );

-- Not in development yet

-- create table task(
--     id serial not null PRIMARY KEY,
--     description text not null,
--     due_date timestamp with time zone,
--     completed_date timestamp with time zone
-- );

-- create table task_comment(
--     id serial not null PRIMARY KEY,
--     comment text not null,
--     created_on timestamp with time zone not null,
--     created_by int not null references user_account(id),
--     mod_on timestamp with time zone not null,
--     mod_by int not null references user_account(id),
--     task_id int not null references task(id),
--     account_id int not null references business_account(id)
-- );

-- create table task_assigned_people(
--     id serial not null PRIMARY KEY,
--     user_id int not null references user_account(id),
--     task_id int not null references task(id)
-- );

-- create table task_asset_link (
--     id serial not null PRIMARY KEY,
--     asset_id int not null references asset(id),
--     task_id int not null references task(id)
-- );