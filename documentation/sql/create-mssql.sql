/*
* MSSQL Create Table Script
* Notes:
* - In a different order to create-pg.sql because this script executes without errors
* - User table is defined with [user] not user because it is a reserved keyword
* - File table is defined with [file] same reason as above
*/ 

CREATE TABLE account_contact (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY, 
    name nvarchar(100) NOT NULL,
    contact_person varchar(100) NOT NULL,
    email nvarchar(100) NOT NULL,
    phone nvarchar(20) NOT NULL,
);

CREATE TABLE settings_plan_details (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    user_limit int NOT NULL,
    plan_cost_monthly money NOT NULL,
    plan_cost_annually money NOT NULL,
    plan_active bit NOT NULL,
);

CREATE TABLE account ( 
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    active bit NOT NULL,
    plan_id int NOT NULL FOREIGN KEY REFERENCES settings_plan_details(id),
    contact_id int NOT NULL FOREIGN KEY REFERENCES account_contact(id),
);

CREATE TABLE person (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    first_name nvarchar(100) NOT NULL,
    last_name nvarchar(100) NOT NULL,
    birthdate datetime NOT NULL,
    gender nvarchar(11) NOT NULL,
    department nvarchar(100) NOT NULL,
    position nvarchar(100),
    shift nvarchar(100),
    location nvarchar(100),
    general_notes nvarchar(2000),
    special_notes nvarchar(2000),
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),  
);

-- review later
CREATE TABLE training (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    name nvarchar(100) NOT NULL,
    type nvarchar(100),
    expires_on datetime,

    person_id int NOT NULL FOREIGN KEY REFERENCES person(id),
);

-- [user] not user because use is a reserved keyword
CREATE TABLE [user] (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    username nvarchar(100) NOT NULL,
    email_phone nvarchar(100) NOT NULL,
    mobile_phone nvarchar(100) NOT NULL,
);

CREATE TABLE settings_modules (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    module_name nvarchar(100) NOT NULL,
    active bit NOT NULL,
);

CREATE TABLE data_collection_group(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    name nvarchar(100) NOT NULL,
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),
    grouping_id int NOT NULL FOREIGN KEY REFERENCES data_collection_group(id),
);

CREATE TABLE data_collection_item(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    name nvarchar(100) NOT NULL,
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),
    grouping_id int NOT NULL FOREIGN KEY REFERENCES data_collection_group(id),
);

CREATE TABLE user_group(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    name nvarchar(100) NOT NULL,
);

CREATE TABLE user_group_members(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    user_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
    user_group_id int NOT NULL FOREIGN KEY REFERENCES user_group(id),
);

CREATE TABLE reference_members(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    reference_type nvarchar(100) NOT NULL,
    reference_link_id int NOT NULL,
    user_group_id int NOT NULL FOREIGN KEY REFERENCES user_group(id),
    user_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
);

CREATE TABLE [file] (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    name nvarchar(50) NOT NULL,
    path nvarchar(260) NOT NULL,
    type nvarchar(20) NOT NULL,
    uploaded bit NOT NULL,
);

CREATE TABLE doc (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    name nvarchar(255) NOT NULL,
    description nvarchar(2000),
    owner_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
    creation_date datetime NOT NULL,
    review_date datetime NOT NULL,
    modifed_date datetime NOT NULL,
    is_draft bit NOT NULL,
    status nvarchar(100) NOT NULL,
    notes nvarchar(2000) NOT NULL,
    review_frequency datetime NOT NULL,
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),
    type_id int NOT NULL FOREIGN KEY REFERENCES data_collection_item(id),
    created_by_person_id int NOT NULL FOREIGN KEY REFERENCES [user](id),

    reference_type nvarchar(100) NOT NULL,
    reference_link_id int NOT NULL,
    file_id NOT NULL FOREIGN KEY REFERENCES [file](id),
);

CREATE TABLE doc_change_requests(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    details nvarchar(2000) NOT NULL,
    reason nvarchar(2000) NOT NULL,
    user_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
    doc_id int NOT NULL FOREIGN KEY REFERENCES doc(id),
    file_id int NOT NULL FOREIGN KEY REFERENCES [file](id),
);

CREATE TABLE doc_approvals(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    reason nvarchar(2000) NOT NULL, 
    approved bit NOT NULL,
    change_request_id int NOT NULL FOREIGN KEY REFERENCES doc_change_requests(id),
    user_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
);

CREATE TABLE doc_links(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    primary_doc_id int NOT NULL FOREIGN KEY REFERENCES doc(id),
    secondary_doc_id int NOT NULL FOREIGN KEY REFERENCES doc(id),
    notes nvarchar(2000) NOT NULL,
);

CREATE TABLE doc_revision_history(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    revision_date datetime NOT NULL,
    revision_no int NOT NULL,
    file_id int NOT NULL FOREIGN KEY REFERENCES [file](id),
    doc_id int NOT NULL FOREIGN KEY REFERENCES doc(id),
);

CREATE TABLE doc_approvers (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    doc_id int NOT NULL FOREIGN KEY REFERENCES doc(id),
);



-- Health and Safety
CREATE TABLE safety_report (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    type int NOT NULL FOREIGN KEY REFERENCES data_collection_item(id),
    date_occurred datetime NOT NULL,
    location int NOT NULL FOREIGN KEY REFERENCES data_collection_item(id),
    employee_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
    supervisor_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
    department int NOT NULL FOREIGN KEY REFERENCES data_collection_item(id),
    what_happened nvarchar(2000) NOT NULL,
    damages nvarchar(2000), 
    damage_description nvarchar(2000),
    harm nvarchar(2000),
    harm_description nvarchar(2000),
    risk_likelihood nvarchar(100),
    risk_harm nvarchar(100),
    corrective_actions nvarchar(2000),
    is_issue_still_present bit,
    account_id int NOT NULL FOREIGN KEY REFERENCES data_collection_item(id),
);

CREATE TABLE safety_report_damage (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    damage nvarchar(2000) NOT NULL,
    safety_report_id int NOT NULL FOREIGN KEY REFERENCES safety_report(id),
);

CREATE TABLE safety_report_sign_off (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    supervisor_sign_off_date datetime,
    supervisor_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
    safety_rep_sign_off_date datetime,
    safety_rep_id int NOT NULL FOREIGN KEY REFERENCES [user](id),
);


CREATE TABLE hazard_register_item (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    register_id int NOT NULL,
    hazard nvarchar(2000) NOT NULL,
    potential_harm nvarchar(2000) NOT NULL,
    hazard_migition nvarchar(2000) NOT NULL,
    controls nvarchar(2000) NOT NULL,
    document_id int references doc(id),
    date_reported datetime,

    location_id int NOT NULL FOREIGN KEY REFERENCES data_collection_item(id),
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),
);




-- Not in development yet

/*CREATE TABLE task(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    description nvarchar NOT NULL,
    due_date datetime,
    completed_date datetime,
);

CREATE TABLE task_comment(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    comment nvarchar NOT NULL,
    created_on datetime NOT NULL,
    created_by int NOT NULL FOREIGN KEY REFERENCES user(id),
    mod_on datetime NOT NULL,
    mod_by int NOT NULL FOREIGN KEY REFERENCES user(id),
    task_id int NOT NULL FOREIGN KEY REFERENCES task(id),
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),
);

CREATE TABLE task_assigned_people(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    user_id int NOT NULL FOREIGN KEY REFERENCES user(id),
    task_id int NOT NULL FOREIGN KEY REFERENCES task(id),
);

CREATE TABLE task_asset_link (
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    asset_id int NOT NULL FOREIGN KEY REFERENCES asset(id),
    task_id int NOT NULL FOREIGN KEY REFERENCES task(id),
);

-- Assets TODO
CREATE TABLE asset(
    id int IDENTITY(1,1) NOT NULL PRIMARY KEY,
    
    account_id int NOT NULL FOREIGN KEY REFERENCES account(id),
);*/
