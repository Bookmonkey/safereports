
create table absence (
    id serial not null primary key,
    absence_date timestamp with time zone not null,
    employee_id int not null,
    expected_return_date timestamp with time zone not null,
    reason text,
    account_id int not null references business_account(id)
);