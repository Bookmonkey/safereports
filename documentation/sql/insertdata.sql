
-- This insert data should get you up and running.
-- Please insert one at a time. As some insert statements depend on values
insert into settings_modules (module_name, module_key, active) 
VALUES 
('People', 'people', true),
('Safety', 'safety', true),
('Users', 'users', true),
('Data', 'data', true),
('Admin', 'admin', true),
('Assets', 'assets', false),
('Tasks', 'tasks', false),
('Documents', 'documents', true);
-- insert into settings_permissions (permission_name, permission_key_name, permission_description, module_id_code) VALUES
-- ('People Basic', 'people_basic', 'Can only View People', 1),
-- ('People Edit', 'people_edit', 'Can Add, Edit and View People', 1),
-- ('People Full', 'people_full', 'Full access to People', 1),
-- ('Safety Basic', 'safety_basic', 'Can View Safety Reports', 2),
-- ('Safety Edit', 'safety_edit', 'Can Add, Edit and View Safety Reports', 2),
-- ('Safety Rep', 'safety_edit', 'Can Add, Edit, View and Sign off on Safety Reports', 2),
-- ('Safety Chair', 'safety_full', 'Can Add, Edit, View and Sign off on Safety Reports', 2); 

insert into settings_plan_details (plan_name,
    user_limit,
    plan_cost_monthly,
    plan_cost_annually,
    plan_active)
VALUES ('Large Business', 30, 50, 600, true);

insert into business_account (active, plan_id)
VALUES(true, 1);

insert into business_account_contact (account_id, name, contact_person, email, phone)
VALUES(1, 'Magus Creative.', 'Danni Dickson', 'danni.dickson@hotmail.com', '0220354676');


insert into data_collection_group(name, key, grouping_id)
VALUES
('People', 'people', null),
('Safety', 'safety', null);

insert into data_collection_group(name, key, grouping_id)
VALUES
('Department', 'department', 1),
('Location', 'location',  1),
('Position', 'position',  1),
('Shift', 'shift', 1),
('Report Type', 'report_type',2);

insert into data_collection_item(name, account_id, grouping_id)
VALUES 
('Office',1,3), 
('Factory',1,3),
('HR', 1,3),
('Napier Office', 1,4),
('Auckland', 1,4),
('Owner',1,5),
('Employee',1,5),
('Day Shift',1,6),
('Night Shift',1,6);


insert into person (first_name, last_name, birthdate, gender, department, position, shift, location, general_notes, special_notes, archived, account_id)
VALUES('Danni','Dickson','1995-01-27 20:48:00+13','Male',1, 7,6, 4,'','',false, 1);

insert into user_account (username, mobile_phone, person_id, password, account_id)
VALUES('dev@maguscreative.com','',1, '$2a$08$b/PznEoqi5dxHmEvZuHZ6.2t9Beg05P.kTTexcl7rEQKqWc434CRu', 1);

insert into user_modules (user_id, module_id) VALUES 
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8);