# Set up
Clone the repo to your machine.

## Tech info
You will need 
* Postgres
* Node 6 + (currently I use node 7)

Note: Global NPM deps:
* Typescript
* vue-cli

## DB
Create the database using the create.sql file in `documentation/sql`. You will also find `insert.sql` which inserts dummy data to get you going.

I called my db safereports, however that does not matter as you will add your db details to the config file.


### Config file
You will need to update the config file in `config/config.js` to match your config. `In the future this will be all done via node_env` and most likely before you get access to this repo.

If there is anything wrong with this document, please update it.