Single
user_limit = 1
storage = 5GB
cost $15 per month

Small
user_limit = 10
storage = 10GB
cost $30 per month

Medium
user_limit = 20
storage = 20GB
cost $60 per month


High
user_limit = 30
storage = 30GB
cost $90 per month


Additional
$5 per addtional user / per GB