## how to use the datatable component

Firstly, import the datatable component
`import DataTable from "@/components/DataTable";`

Then add it to the components defintion:
``` javascript
  components: {
    'data-table':DataTable, // add this
    'searchbox': SearchBox,
  }
  data() { 
    return {...}
  }
```

In the HTML (template) add the following `
<data-table :settings="tableSettings" :data="[DATAVAR]"></data-table>
`

Now add the tableSettings and the DATAVAR to the data object. Please note: DATAVAR is just for this document, rename it to something more clear.
``` javascript
data() {
  return {
    ... ,
    DATAVAR: [],
    tableSettings: {
      viewLink: {
        state: true,
        key: "id",
        url: "/app/people/view"
      },
      columns: [
        { 
          key: "full_name", // required - The data key used to define the value in the table row
          human: "Name", // required - The table column heading
          sortable: true // optional - allows you to sort the column by ASC or DESC
          isDate: true // optional - formats dates in 'dd/mm/yyyy'
        },
        ...n, // copy above for more columns
      ],

      maxNumber: 10, // how many items will appear.
      loading: true // when fetching or updating the data use this variable to trigger the loading alert.
    },
  }
}
```

When you are fetching or updating the DATAVAR set, use the `tableSettings.loading` to trigger the loading alert message.