## How to use the Alert component
First, import the component along with the Common utils import. If you have used the vue.vue 'template' this should already be included.
``` javascript
import { Common } from "@/common";

import AlertMessage from "@/components/AlertMessage";
```
Add the component to the template (HTML `<alert v-bind:state="state"></alert>`

Add the `state` object in the data object in the vue template. Like so:
``` javascript
data() {
  return {
    ... ,
    state: Common.updateAlertState(
      "success",
      "Success",
      "This is a successful message",
      false
    ),
  }
}
```

Every time you need to bring the alert out use the `Common.updateAlertState()` function
