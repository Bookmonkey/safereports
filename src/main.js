import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/index";

Vue.config.productionTip = false;

import * as moment from "moment";
Vue.filter("date", date => {
  return moment(date).format("DD/MM/YYYY");
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
