import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";

import RegisterAccount from "./views/Register";

import AppWrapper from "./views/app/Wrapper.vue";
import AppDashboard from "./views/app/Dashboard.vue";
import NotificationDashboard from "@/views/app/NotificationDashboard.vue";

// Incidents
import AppIncidentsViewAll from "./views/app/incidents/IncidentsViewAll.vue";
import AppIncidentsViewSingle from "./views/app/incidents/IncidentsViewSingle.vue";
import AppIncidentsAdd from "./views/app/incidents/IncidentsAdd.vue";

import IncidentsInvestigationAdd from "./views/app/incidents/investigation/InvestigationAdd.vue";

// Hazard Registers
import AppRegistersViewAll from "./views/app/registers/RegistersViewAll.vue";
import AppRegistersViewSingle from "./views/app/registers/RegistersViewSingle.vue";
import AppRegistersAdd from "./views/app/registers/RegistersAdd.vue";

// People
import AppPeopleViewSingle from "./views/app/staff/people/PeopleViewSingle.vue";
import AppPeopleViewAll from "./views/app/staff/people/PeopleViewAll.vue";
import AppPeopleAdd from "./views/app/staff/people/PeopleAdd.vue";

import AppStaffAbsencesViewAll from "@/views/app/staff/absences/AbsencesViewAll.vue";
import AppStaffAbsencesAdd from "@/views/app/staff/absences/AbsencesAdd.vue";
import AppStaffQualificationsViewAll from "@/views/app/staff/qualifications/QualificationsViewAll.vue";


import SuppliersAdd from "@/views/app/suppliers/SuppliersAdd";
import SuppliersViewAll from "@/views/app/suppliers/SuppliersViewAll";
import SuppliersViewSingle from "@/views/app/suppliers/SuppliersViewSingle";

import AssetsAdd from "./views/app/assets/AssetsAdd.vue";
import AssetsViewAll from "./views/app/assets/AssetsViewAll.vue";
import AssetsViewSingle from "./views/app/assets/AssetsViewSingle.vue";

import SafetyAnalytics from "@/views/app/analytics/Safety.vue";

// users
import AppUsersViewSingle from "./views/app/users/UsersViewSingle.vue";
import AppUsersViewAll from "./views/app/users/UsersViewAll.vue";
import AppUsersAdd from "./views/app/users/UsersAdd.vue";

import DataCollections from "./views/app/data/view.vue";

// Admin
import AdminWrapper from "./views/app/admin/AdminWrapper.vue";
import AdminDetails from "./views/app/admin/AdminDetails.vue";
import AdmimPayments from "./views/app/admin/AdminPayments.vue";

import Notfound from "./views/app/NotFound";



// 
import DeveloperAdminWrapper from "@/views/admin/Wrapper";

import store from "./store/index";

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        title: "Safe Reports - Homepage"
      }
    },
    {
      path: "/login",
      name: "login",
      component: Login,
      meta: {
        title: "Safe Reports - Login"
      }
    },
    {
      path: "/register",
      name: "RegisterAccount",
      component: RegisterAccount,
      meta: {
        title: "Safe Reports - Register account"
      }
    },
    {
      path: "/app",
      name: "App",
      component: AppWrapper,
      beforeEnter: (to, from, next) => {
        if (store.getters.getAuthState.loggedIn) return next();
        else {
          store
            .dispatch("checkLoginStatus")
            .then(() => {
              return next();
            })
            .catch(() => {
              window.location.href = "/";
            });
        }
      },
      children: [
        {
          path: "dashboard",
          name: "AppDashboard",
          component: AppDashboard,
          meta: {
            title: "Safe Reports - Dashboard"
          }
        },
        {
          path: "dashboard/notifications",
          name: "NotificationDashboard",
          component: NotificationDashboard,
          meta: {
            title: "Safe Reports - Notification dashboard"
          }
        },
        {
          path: "incidents",
          name: "AppIncidentsViewAll",
          component: AppIncidentsViewAll,
          meta: {
            title: "Safe Reports - View Incidents"
          }
        },
        {
          path: "incidents/view/:id",
          name: "AppIncidentViewSingle",
          component: AppIncidentsViewSingle,
          meta: {
            title: "Safe Reports - View Incident"
          }
        },
        {
          path: "incidents/add",
          name: "AppIncidentsAdd",
          component: AppIncidentsAdd,
          meta: {
            title: "Safe Reports - Add Incident"
          }
        },
        {
          path: "incidents/investigation/new/:id",
          name: "IncidentsInvestigationAdd",
          component: IncidentsInvestigationAdd,
          meta: {
            title: "Safe Reports - Add investigation",
          }
        },
        {
          path: "registers",
          name: "AppRegistersViewAll",
          component: AppRegistersViewAll,
          meta: {
            title: "Safe Reports - Registers"
          }
        },
        {
          path: "registers/view/:id",
          name: "AppRegistersViewSingle",
          component: AppRegistersViewSingle,
          meta: {
            title: "Safe Reports - Registers"
          }
        },
        {
          path: "registers/add",
          name: "AppRegistersAdd",
          component: AppRegistersAdd,
          meta: {
            title: "Safe Reports - Add Registers"
          }
        },
        {
          path: "people",
          name: "AppPeopleViewAll",
          component: AppPeopleViewAll,
          meta: {
            title: "Safe Reports - View People"
          }
        },
        {
          path: "people/view/:id",
          name: "AppPeopleViewSingle",
          component: AppPeopleViewSingle,
          meta: {
            title: "Safe Reports - People"
          }
        },
        {
          path: "people/add",
          name: "AppPeopleAdd",
          component: AppPeopleAdd,
          meta: {
            title: "Safe Reports - Add Person"
          }
        },
        {
          path: "staff/absences",
          name: "AppStaffAbsencesViewAll",
          component: AppStaffAbsencesViewAll,
          meta: {
            title: "SafeReports - View absences"
          }
        },
        {
          path:"staff/absences/add",
          name: "AppStaffAbsencesAdd",
          component: AppStaffAbsencesAdd,
          meta: {
            title: "SafeReports - Add absence"
          }
        },
        {
          path: "staff/qualifications",
          name: "AppStaffQualificationsViewAll",
          component: AppStaffQualificationsViewAll
        },
        {
          path: "assets",
          name: "AssetsViewAll",
          component: AssetsViewAll,
          meta: {
            title: "Safe Reports - Assets"
          }
        },
        {
          path: "assets/add",
          name: "AssetsAdd",
          component: AssetsAdd,
          meta: {
            title: "Safe Reports - Add Assets"
          }
        },
        {
          path: 'suppliers',
          name: "SuppliersViewAll",
          component: SuppliersViewAll,
          meta: {
            title: "Safe Reports - View suppliers"
          }
        },
        {
          path: "suppliers/view/:id",
          name: "SuppliersViewSingle",
          component: SuppliersViewSingle,
          meta: {
            title: "Safe Reports - View suppliers"
          }
        },
        {
          path: 'suppliers/add',
          name: "SuppliersAdd",
          component: SuppliersAdd,
          meta: {
            title: "Safe Reports - Add supplier"
          }
        },
        {
          path: "assets/view/:id",
          name: "AssetsViewSingle",
          component: AssetsViewSingle,
          meta: {
            title: "Safe Reports - Viewing single asset"
          }
        },
        {
          path: "analytics/safety",
          name: "SafetyAnalytics",
          component: SafetyAnalytics,
          meta: {
            title: "Safe Reports - Analytics"
          }
        },
        {
          path: "users",
          name: "AppUsersViewAll",
          component: AppUsersViewAll,
          meta: {
            title: "Safe Reports - Users"
          }
        },
        {
          path: "users/view/:id",
          name: "AppUsersViewSingle",
          component: AppUsersViewSingle,
          meta: {
            title: "Safe Reports - Viewing a user"
          }
        },
        {
          path: "users/add",
          name: "AppUsersAdd",
          component: AppUsersAdd,
          meta: {
            title: "Safe Reports - Add a user"
          }
        },

        {
          path: "datacollections",
          name: "DataCollections",
          component: DataCollections,
          meta: {
            title: "Safe Reports - Datacollections"
          }
        },
        {
          path: "admin",
          name: "AdminWrapper",
          component: AdminWrapper,
          children: [
            {
              path: "",
              name: "AdminDetails",
              component: AdminDetails,
              meta: {
                title: "Safe Reports - Admin Details"
              }
            },
            {
              path: "payments",
              name: "AdminPayments",
              component: AdmimPayments,
              meta: {
                title: "Safe Reports - Admin Payments"
              }
            }
          ]
        }
      ]
    },
    {
      path: "/admin",
      name: "DevAdmin",
      component: DeveloperAdminWrapper,
      meta: {
        title: "SafeReports Developer Interface"
      },
      beforeEnter: (to, from, next) => {
        if (store.getters.getAuthState.loggedIn) return next();
        else {
          store
            .dispatch("checkIfUserIsDeveloper")
            .then(result => {
              if (!result) {
                window.location.href = "/"
              } else {
                return next();
              }
            })
            .catch(() => {
              window.location.href = "/";
            });
        }
      },
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});
router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

export default router;
