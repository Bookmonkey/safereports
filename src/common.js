export let Common = {
  setAlertState () {
    return {
      type: '',
      tag: '',
      message: '',
      show: false
    };
  },
  updateAlertState(type, tag, message, show) {
    return {
      type: type,
      tag: tag,
      message: message,
      show: show
    };
  },

  /**
   * Close the alert 
   * @param state the state object
   * @param time default should be 5000 (5 seconds)
   */
  closeAlertState(state, time) {
    return setTimeout(() => {
      state.show = false;
      clearTimeout();
    }, time);
  },

  // UI State inputs
  isActiveUIState(state, newState) {
    return state === newState ? "active" : "";
  }
};
