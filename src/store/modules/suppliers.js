import axios from "axios";
const api = "/api";

const state = {
  suppliers: [],
};
const getters = {};
const actions = {
  getSuppliers() {
    return axios.get(`${api}/suppliers/get`);
  },

  getSupplierById({ commit }, id) {
    return axios.get(`${api}/suppliers/get/${id}`);
  },

  addSupplier({ commit }, supplier) {
    return axios.post(`${api}/suppliers/add`, { supplier });
  },

  updateSupplier({ commit }, supplier) {
    return axios.post(`${api}/suppliers/update`, { supplier });
  },

  deleteSupplierById({ commit }, id){
    return axios.get(`${api}/suppliers/delete/${id}`);
  }
};
const mutations = {};


export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
