import axios from "axios";
const api = "/api";

const state = {
  safety: [],
  hazardRegisters: []
};
const getters = {};
const actions = {
  getSafetyReports({ commit }) {
    return axios.get(`${api}/safety/get`).then(response => {
      commit("SET_SAFETY_REPORTS", { safety: response.data });
      return response;
    });
  },

  getSafetyReportById({ commit }, id) {
    return axios.get(`${api}/safety/get/${id}`).then(response => {
      return response;
    });
  },

  addSafetyReport({ commit }, safety) {
    return axios.post(`${api}/safety/add`, { safety: safety }).then(
      response => {
        commit("ADD_SAFETY_REPORT", { safety: safety });
        return response;
      },
      error => {
        // handle error
      }
    );
  },

  updateSafetyReport({ commit }, safety) {
    return axios.post(`${api}/safety/update`, { safety: safety });
  },

  addSafetyReportInvestigation({ commit }, investigation) {
    return axios.post(`${api}/safety/investigation/add`, {
      investigation: investigation
    });
  },
  updateSafetyReportInvestigation({ commit }, investigation) {
    return axios.post(`${api}/safety/investigation/update`, {
      investigation: investigation
    });
  },

  addHazardLinksToIncident({ commit }, hazardLinks) {
    return axios.post(`${api}/safety/links/add`, {
      hazardLinks: hazardLinks
    });
  },
  removeHazardLinkFromIncident({ commit }, linkID) {
    return axios.get(`${api}/safety/links/remove/${linkID}`);
  },

  // Hazard Registers
  getHazardRegisters({ commit }) {
    return axios.get(`${api}/safety/registers/view`).then(response => {
      commit("SET_HAZARD_REGISTERS", { registers: response.data });
      return response;
    });
  },
  getHazardById({ commit }, id) {
    return axios.get(`${api}/safety/registers/view/${id}`);
  },

  addRegister({ commit }, registerItem) {
    return axios.post(`${api}/safety/registers/add`, {
      register: registerItem
    });
  },

  updateHazardRegister({ commit }, registerItem) {
    return axios.post(`${api}/safety/registers/update`, {
      register: registerItem
    });
  },

  archiveHazardRegisterItem({ commit }, archivedObject) {
    return axios.get(
      `${api}/safety/registers/archive/${archivedObject.id}/${
        archivedObject.archive
      }`
    );
  },
};
const mutations = {
  SET_SAFETY_REPORTS: (state, { safety }) => {
    state.safety = safety;
  },

  ADD_SAFETY_REPORT: (state, { safety }) => {
    state.safety.push(safety);
  },

  SET_HAZARD_REGISTERS: (state, { registers }) => {
    state.hazardRegisters = registers;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
