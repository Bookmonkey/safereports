import axios from "axios";
const api = "/api";

const state = {
  people: []
};
const getters = {
  getAllPeople: state => {
      return state.people;
    }
};
const actions = {
  // People
  getPeople({ commit, state }) {
    return axios.get(`${api}/people/get`).then(response => {
        commit("SET_PEOPLE", { people: response.data });
        return state.people;
    })
  .catch(error => {
        return error;
    });
  },

  getPerson({ commit }, id) {
    return axios.get(`${api}/people/get/${id}`);
  },

  getPeopleNames() {
    return axios.get(`${api}/people/get/names`);
  },

  savePerson({ commit }, person) {
    return axios.post(`${api}/people/add`, { person: person }).then(
      response => {
        commit("ADD_PERSON", { person: person });
        return response;
      },
      error => {
        return error;
      }
    );
  },

  updatePerson({ commit }, person) {
    return axios
      .post(`${api}/people/update/${person.id}`, { person: person })
      .then(response => {
        return response;
      });
  },
  deletePerson({ commit }, id) {
    return axios.get(`${api}/people/delete/${id}`);
  },

  archivePerson({ commit }, archiveObject) {
    return axios
      .get(
        `${api}/people/archive/${archiveObject.id}/${archiveObject.archived}`
      )
      .then(response => response);
  },

  addTrainingToPerson({ commit }, training) {
    return axios.post(`${api}/people/training/add`, {
      training: training
    });
  },

  updateTraining({ commit }, training) {
    return axios.post(`${api}/people/training/update`, {
      training: training
    });
  },
  deleteTrainingItem({ commit }, trainingId) {
    return axios.get(`${api}/people/training/delete/${trainingId}`);
  },
  getAbsences({ commit }) {
    return axios.get(`${api}/people/absence/get`);
  },
  addAbsence({ commit }, absence) {
    return axios.post(`${api}/people/absence/add`, { absence });
  },
};

const mutations = {
  SET_PEOPLE: (state, { people }) => {
    state.people = people;
  },
  ADD_PERSON: (state, { person }) => {
    state.people.push(person);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
