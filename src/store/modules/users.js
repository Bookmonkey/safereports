import axios from "axios";
const api = "/api";

const state = {
  users: []
};
const getters = {};
const actions = {
  getUsers({ commit }) {
    return axios.get(`${api}/users/get`).then(response => {
      commit("SET_USERS", { users: response.data });
      return response.data;
    });
  },

  getUserById({ commit }, id) {
    return axios.get(`${api}/users/get/${id}`).then(response => {
      return response.data;
    });
  },

  addUser({ commit }, user) {
    return axios.post(`${api}/users/add`, { user: user }).then(response => {
      // console.log(response);
    });
  },

  updateUser({ commit }, user) {
    return axios.post(`${api}/users/update`, { user: user });
  },

  deleteUserById({ commit }, id) {
    return axios.get(`${api}/users/delete/${id}`);
  },

  addUserModules({ commit }, user) {
    return axios
      .post(`${api}/users/add/modules/${user.id}`, { modules: user.modules })
      .then(response => response.data);
  },

  addModuleToUser({ commit }, info) {
    return axios
      .get(`${api}/users/add/module/${info.userId}/${info.moduleId}`)
      .then(response => response.data);
  },
  removeModuleFromUser({ commit }, info) {
    return axios
      .get(`${api}/users/remove/module/${info.userId}/${info.moduleId}`)
      .then(response => response.data);
  },
};
const mutations = {
  SET_USERS: (state, { users }) => {
    state.users = users;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
