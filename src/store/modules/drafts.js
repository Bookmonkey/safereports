import axios from "axios";
const api = "/api";

const state = {};
const getters = {};
const actions = {
  getDrafts({ commit }, location) {
    return axios.get(`${api}/drafts/get/all/${location}`);
  },

  getDraftById({ commit }, draftId) {
    return axios.get(`${api}/drafts/get/${draftId}`);
  },

  addDraft({ commit }, draft) {
    return axios.post(`${api}/drafts/add`, { draft: draft });
  },

  updateDraft({ commit }, draft) {
    return axios.post(`${api}/drafts/update`, { draft: draft });
  },
  deleteDraft({ commit }, draftId) {
    return axios.get(`${api}/drafts/delete/${draftId}`);
  }
};
const mutations = {};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};