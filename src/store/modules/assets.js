import axios from "axios";
const api = "/api";

const state = {
  assets: []
};
const getters = {};
const actions = {
  addAsset({ commit }, asset) {
    return axios.post(`${api}/assets/add`, { asset: asset }).then(response => {
      commit("ADD_ASSET", { asset: asset });
      return response;
    });
  },

  getAssets({ commit }) {
    return axios.get(`${api}/assets/get`).then(response => {
      return response;
    });
  },

  getAssetById({ commit }, id) {
    return axios.get(`${api}/assets/get/${id}`);
  },

  updateAssetById({ commit }, asset) {
    return axios.post(`${api}/assets/update`, { asset: asset });
  },

  addServiceLogToAsset({ commit }, service) {
    return axios.post(`${api}/assets/add/service`, { service: service });
  },

  deleteAssetById({ commit }, id) {
    return axios.get(`${api}/assets/delete/${id}`);
  }
};

const mutations = {
  ADD_ASSET: (state, { asset }) => {
    state.assets.push(asset);
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
