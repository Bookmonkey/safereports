import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

import staff from "./modules/staff";
import safety from "./modules/safety";
import assets from "./modules/assets";
import suppliers from "./modules/suppliers";
import drafts from "./modules/drafts";
import users from "./modules/users";

import utils from "./modules/utils";

axios.defaults.withCredentials = true; // enable axios post cookie, default false
Vue.use(Vuex);
const api = "/api";

export default new Vuex.Store({
  modules: {
    staff,
    safety,
    assets,
    suppliers,
    drafts,
    users,
    utils
  },
  state: {
    auth: {
      loggedIn: false,
      user: []
    },
    optionalFields: {
      people: {
        id_code: { human: "ID Code", state: true },
        position: { human: "Position", state: true },
        shift: { human: "Shift", state: true },
        location: { human: "Location", state: true },
        gender: { human: "Gender", state: false },
        birthdate: { human: "DOB", state: false },
        phone: { human: "Phone number", state: false },
        email: { human: "Email address", state: false },
        generalNotes: { human: "General Notes", state: false },
        specialNotes: { human: "Specical Notes", state: false }
      },
      assets: {
        manfacture_date: { human: "Manfacture date", state: false },
        wof_date: { human: "WOF Expiry", state: false },
        cof_date: { human: "COF Expiry", state: false },
        next_service_date: { human: "Next Service", state: false }
      }
    }
  },

  mutations: {
    LOGGED_IN: (state, { user }) => {
      state.auth.loggedIn = true;
      state.auth.user = user;
    },
    LOGGED_OUT: state => {
      state.auth.loggedIn = false;
      state.auth.user = [];
    }
  },
  actions: {
    getDashboardStats({ commit }, period) {
      return axios.get(`${api}/dashboard/stats/${period}`).then(response => {
        return response.data;
      });
    },

    getDashboardNotifications({ commit }) {
      return axios.get(`${api}/dashboard/notifications/summary`);
    },

    getAllNotifications({ commit }) {
      return axios.get(`${api}/dashboard/notifications/all`)
    },

    // Admin
    getAdminAccount({ commit }) {
      return axios.get(`${api}/admin/account`);
    },
    updateAdminAccount({ commit }, adminAccount) {
      return axios.post(`${api}/admin/account/update`, {
        account: adminAccount
      });
    },
    getAllDataCollections({ commit }) {
      return axios.get(`${api}/admin/data/get/collections`);
    },

    getDataCollections({ commit }, collections) {
      return axios.post(`${api}/admin/data/get/collection/items`, {
        collections: collections
      });
    },

    getDataCollectionById({ commit }, id) {
      return axios.get(`${api}/admin/data/get/collection/${id}`);
    },

    addDataItem({ commit }, item) {
      return axios.post(`${api}/admin/data/add/${item.id}`, {
        item: item.value
      });
    },

    updateDataItem({ commit }, item) {
      return axios.post(`${api}/admin/data/update/item`, { item: item });
    },
    deleteDataItem({ commit }, item) {
      return axios.get(
        `${api}/admin/data/delete/item/${item.id}/${item.account_id}`
      );
    },

    // modules
    getSettingsModules({ commit }) {
      return axios.get(`${api}/users/settingsmodules`);
    },
    // Login

    loginUser({ commit }, user) {
      return new Promise((resolve, reject) => {
        axios
          .post(`${api}/auth/login`, user)
          .then(response => {
            commit("LOGGED_IN", { user: response.data });
            resolve(true);
          })
          .catch(error => reject(false));
      });
    },
    logout({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`${api}/auth/logout`)
          .then(response => {
            commit("LOGGED_OUT");
            resolve(true);
          })
          .catch(error => reject(error));
      });
    },

    checkLoginStatus({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`${api}/auth/loginStatus`)
          .then(response => {
            commit("LOGGED_IN", { user: response.data });
            resolve(true);
          })
          .catch(error => reject(false));
      });
    },

    checkIfUserIsDeveloper({ commit }) {
      return new Promise((resolve, reject) => {
        axios
          .get(`${api}/auth/loginStatus`)
          .then(response => {
            if (response.data.is_developer) {
              resolve(true);
            } else {
              resolve(false);
            }
            // commit("LOGGED_IN", { user: response.data });
            resolve(true);
          })
          .catch(error => reject(false));
      });
    },

    getSubscriptionPlans({ commit }) {
      return axios.get(`${api}/admin/list/plans`);
    },

    registerAccount({ commit }, form) {
      axios.post(`${api}/admin/register/account`, { form: form });
    }
  },
  getters: {
    getOptionalFields: state => location => {
      return state.optionalFields[location];
    },

    getAuthState: state => {
      return state.auth;
    },

    getIsDeveloper: state => {
      return state.auth.user.is_developer;
    },

    searchDataFor: (state, getters) => (
      stateName,
      searchValue,
      searchKey,
      filters
    ) => {
      const checkForShownFiltersThatContainValues = filters => {
        let keys = [];
        if (filters) {
          keys = Object.keys(filters);
        }

        let hasFilters = false;
        keys.map(ele => {
          if (
            !hasFilters &&
            filters[ele].shown &&
            filters[ele].selected_value.name !== undefined
          )
            hasFilters = true;
        });

        return hasFilters;
      };
      let searchingOnState = null;

      switch (stateName) {
        case "people":
          searchingOnState = staff.state.people;
          break;
        case "safety":
          searchingOnState = safety.state.safety;
          break;
        case "registers":
          searchingOnState = safety.state.hazardRegisters;
          break;
        case "assets":
          searchingOnState = assets.state.assets;
          break;
        case "users":
          searchingOnState = users.state.users;
          break;
      }

      let newResults = searchingOnState.filter(ele => {
        if (ele[searchKey]) {
          if (
            ele[searchKey].toLowerCase().indexOf(searchValue.toLowerCase()) > -1
          )
            return ele;
        } else {
          return ele;
        }
      });

      let hasFilters = checkForShownFiltersThatContainValues(filters);

      if (hasFilters) {
        newResults = newResults.filter(ele => {
          for (const key in filters) {
            let filter = filters[key];

            if (filter.selected_value && filter.selected_value.name) {
              if (ele[key].name === null) return ele;

              if (ele[key].name === filter.selected_value.name) return ele;
            }
          }
        });
      }

      return newResults;
    },

    getPeopleByFilter: (state, getters) => (key, value) => {
      return state.people.filter(ele => {
        if (ele[key] === value) return ele;
      });
    }
  }
  
});
